package com.camness.vidimguploadjun2016.trips;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.database.LocalDBManager;
import com.camness.vidimguploadjun2016.network.response.Trips;

import java.util.ArrayList;

public class SharedTripsActivity extends Fragment {

    private ArrayList<Trips> sharedTrips = new ArrayList<>();
    private View view;
    private TextView tvNoTrips;
    private RecyclerView recyclerView;
    private TripsRecyclerAdapter tripsRecyclerAdapter;

    public static SharedTripsActivity newInstance() {
        return new SharedTripsActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_shared_trips, container, false);
        initUI();
        setSharedTrips();
        if (sharedTrips.size() == 0) {
            tvNoTrips.setVisibility(View.VISIBLE);
        } else {
            setAdapter();
        }
        setAdapter();

        return view;
    }

    private void initUI() {
        recyclerView = (RecyclerView) view.findViewById(R.id.myTripsList);
        tvNoTrips = (TextView) view.findViewById(R.id.noTrips);
    }

    private void setAdapter() {
        tripsRecyclerAdapter = new TripsRecyclerAdapter(getContext(), sharedTrips);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(tripsRecyclerAdapter);
    }

    private void setSharedTrips () {
        ArrayList<Trips> allTrips = LocalDBManager.getTrips();
        for (int i = 0; i < allTrips.size(); i++){
            if (!allTrips.get(i).isOwn()){
                sharedTrips.add(allTrips.get(i));
            }
        }
    }
}
