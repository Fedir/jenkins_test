package com.camness.vidimguploadjun2016.trips;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.network.CamnessApiService;
import com.camness.vidimguploadjun2016.network.CancelableCallback;
import com.camness.vidimguploadjun2016.network.response.UploadResponse;
import com.camness.vidimguploadjun2016.network.response.UploadedFile;
import com.camness.vidimguploadjun2016.network.response.UploadedFilesResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTrip extends AppCompatActivity {

    private static final String KEY_ACTIVITY = "activity";
    private List<UploadedFile> uploadedFiles = new ArrayList<>();
    private MyUploadedFilesRecyclerAdapter mRecyclerAdapter;
    private RecyclerView mRecyclerView;
    private EditText etName;
    private EditText etDescription;
    private Button btnSave;
    private Button btnCancel;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnSave:
                    uploadTrip();
                    break;
                case R.id.btnCancel:
                    onBackPressed();
                    break;
            }
        }
    };
    private CancelableCallback callback = new CancelableCallback() {
        @Override
        public void onResponse(Call call, Response response) {
            UploadedFilesResponse filesResponse = (UploadedFilesResponse) response.body();
            uploadedFiles = filesResponse.getData();
        }

        @Override
        public void onFailure(Call call, Throwable t) {
        }
    };

    private Callback<UploadResponse> uploadResponseCallback = new Callback<UploadResponse>() {
        @Override
        public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
            Toast.makeText(CreateTrip.this, "Trip uploaded successfully", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(Call<UploadResponse> call, Throwable t) {
            Toast.makeText(CreateTrip.this, "ERROR", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_trip);
        uploadedFiles.add(new UploadedFile( "1","trip", "10-20-2088", "10:10", "video", "test@mail.com","mult1"));
        uploadedFiles.add(new UploadedFile( "2","trip", "10-20-2088", "10:10", "video", "test@mail.com","mult2"));
        uploadedFiles.add(new UploadedFile( "3","trip", "10-20-2088", "10:10", "video", "test@mail.com","mult3"));
        uploadedFiles.add(new UploadedFile( "4","trip", "10-20-2088", "10:10", "video", "test@mail.com","mult4"));
        uploadedFiles.add(new UploadedFile( "5","trip", "10-20-2088", "10:10", "video", "test@mail.com","mult5"));

        initUI();
        setAdapter();
    }

    private void initUI() {
        etName = (EditText) findViewById(R.id.etName);
        etDescription = (EditText) findViewById(R.id.etDescription);
        mRecyclerView = (RecyclerView) findViewById(R.id.rvMyUploadedFilesList);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSave.setOnClickListener(onClickListener);
        btnCancel.setOnClickListener(onClickListener);
    }

    private void setAdapter() {
        mRecyclerAdapter = new MyUploadedFilesRecyclerAdapter(uploadedFiles);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    private void uploadTrip() {
        String tripName = etName.getText().toString();
        String tripDescription = etDescription.getText().toString();
//        CamnessApiService.uploadTrip(mRecyclerAdapter.getTripData(),
//                tripName, tripDescription, uploadResponseCallback);
    }

    private void getUploadedTrips() {
       CamnessApiService.getUploadedFiles(callback);
    }
}
