package com.camness.vidimguploadjun2016.trips;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.network.CamnessApiService;
import com.camness.vidimguploadjun2016.network.CancelableCallback;

import retrofit2.Call;
import retrofit2.Response;

public class ShareTrip extends AppCompatActivity {

    private static final String KEY_TRIP_ID = "tripId";
    private EditText etShareMail;
    private Button btnShare;
    private CancelableCallback callback = new CancelableCallback() {
        @Override
        public void onFailure(Call call, Throwable t) {
            Toast.makeText(ShareTrip.this, "ERROR", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onResponse(Call call, Response response) {
        }
    };
    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            shareTrip();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_trip);
        initUi();
    }

    private void initUi() {
        etShareMail = (EditText) findViewById(R.id.etShareMail);
        btnShare = (Button) findViewById(R.id.btnShare);
        btnShare.setOnClickListener(listener);
    }

    private void shareTrip() {
        if (getIntent().getStringExtra(KEY_TRIP_ID) != null) {
            String tripId = getIntent().getStringExtra(KEY_TRIP_ID);
            String userMail = etShareMail.getText().toString();
//            CamnessApiService.shareTrip(tripId, userMail, callback);
        }
    }
}
