package com.camness.vidimguploadjun2016.trips;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.database.LocalDBManager;
import com.camness.vidimguploadjun2016.main.OnItemChangeCallback;
import com.camness.vidimguploadjun2016.network.CamnessApiService;
import com.camness.vidimguploadjun2016.network.CancelableCallback;
import com.camness.vidimguploadjun2016.network.response.Trips;
import com.camness.vidimguploadjun2016.network.response.TripsResponse;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class TripsFragment extends Fragment {

    private OnItemChangeCallback mFragmentsListener;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<Trips> trips = new ArrayList<>();
    private TripsPagerAdapter pagerAdapter;
    private View view;
    private FloatingActionButton actionButton;
    private ArrayList<String> multiCoord = new ArrayList<>();

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getContext(), CreateTrip.class);
            startActivity(intent);
        }
    };

    private CancelableCallback callback = new CancelableCallback() {
        @Override
        public void onFailure(Call call, Throwable t) {
        }

        @Override
        public void onResponse(Call call, Response response) {
            LocalDBManager.deleteTripsResponse();
            LocalDBManager.deleteTrips();
            LocalDBManager.saveTripsResponse(new TripsResponse(true, trips, 1));
        }
    };

    public static TripsFragment newInstance() {
        return new TripsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_trips, container, false);
        initUI();
        setToolbar();
        setAdapter();
        multiCoord.add("oyaoH_`nqCXaAd@sCXHVBRDXRTPADGb@ATD^NRp@h@fBjA~BrApAn@v@XhIjAM~BbANP@L?FDHVDJHHVHl@N");
        addTrips();
        saveTrips();
//        multiCoord.add("cv`oHgomqC");
//        multiCoord.add("sn|nHcjoqC");
//        multiCoord.add("ss`oHinmqCoA]");
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemChangeCallback) {
            mFragmentsListener = (OnItemChangeCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentsListener = null;
    }

    private void initUI() {
        actionButton = (FloatingActionButton) view.findViewById(R.id.addTrip);
        actionButton.setOnClickListener(onClickListener);
        pagerAdapter = new TripsPagerAdapter(getFragmentManager(), getContext());
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.pagerTrips);
    }

    private void setToolbar() {
        mFragmentsListener.setTitleToolbar(getString(R.string.trips_title));
        mFragmentsListener.showToolbar();
        mFragmentsListener.enabledSwipeDrawer();
        mFragmentsListener.setDrawerMenu();
        mFragmentsListener.setSelectedNavigationIndex(2);
    }

    private void setAdapter() {
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void addTrips() {
//        CamnessApiService.getTrips(callback);
        trips.add(new Trips("1", "trip1", "description1", true, "link1", multiCoord));
        trips.add(new Trips("2", "trip2", "description2", false, "link2", multiCoord));
        trips.add(new Trips("3", "trip3", "description3", false, "link3", multiCoord));
        trips.add(new Trips("4", "trip4", "description4", true, "link4", multiCoord));
        trips.add(new Trips("5", "trip5", "description5", true, "link5", multiCoord));
    }

    private void saveTrips() {
        LocalDBManager.deleteTripsResponse();
        LocalDBManager.deleteTrips();
        LocalDBManager.saveTripsResponse(new TripsResponse(true, trips, 1));
    }
}
