package com.camness.vidimguploadjun2016.trips;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.camness.vidimguploadjun2016.R;

import java.util.ArrayList;

public class TripsPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> mFragmentList = new ArrayList<>();
    private ArrayList<String> mFragmentTitleList = new ArrayList<>();

    public TripsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mFragmentList.add(MyTripsFragment.newInstance());
        mFragmentList.add(SharedTripsActivity.newInstance());
        mFragmentTitleList.add(context.getString(R.string.my_trips));
        mFragmentTitleList.add(context.getString(R.string.shared_trips));
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
