package com.camness.vidimguploadjun2016.trips;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.network.CancelableCallback;
import com.camness.vidimguploadjun2016.network.response.Trips;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class TripsRecyclerAdapter extends RecyclerView.Adapter<TripsRecyclerAdapter.TripViewHolder> {

    private static final String KEY_TRIP_ID = "tripId";
    private static final String KEY_COORD = "coord_key";

    private List<Trips> trips = new ArrayList<>();
    private Context context;

    private CancelableCallback cancelCallback = new CancelableCallback() {
        @Override
        public void onFailure(Call call, Throwable t) {
            Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onResponse(Call call, Response response) {
            notifyDataSetChanged();
        }
    };

    public TripsRecyclerAdapter(Context context, ArrayList<Trips> trips) {
        this.context = context;
        this.trips = trips;
    }

    @Override
    public TripViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip, parent, false);
        TripViewHolder tvh = new TripViewHolder(view);
        return tvh;
    }

    @Override
    public void onBindViewHolder(TripViewHolder holder, int position) {
        String tripId = trips.get(position).getTripId();
        String s = generateDynamicLink(tripId);
        holder.tvLink.setText(s);
        Log.i("dynamicLinkUri", "onBindViewHolder: " + s);
        holder.tvName.setText(trips.get(position).getTripName());
        holder.tvDescription.setText(trips.get(position).getTripDescription());
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    class TripViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView tvLink, tvName, tvDescription, tvShare, tvDelete;
        String tripId;
        ArrayList<String> multiCoord = new ArrayList<>();
        private View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.link:
                        Intent linkIntent = new Intent(context, HyperLinkActivity.class);
//                        multiCoord = trips.get(getAdapterPosition()).getMultiCoords();
//                        multiCoord.add("oyaoH_`nqCXaAd@sCXHVBRDXRTPADGb@ATD^NRp@h@fBjA~BrApAn@v@XhIjAM~BbANP@L?FDHVDJHHVHl@N");
                        multiCoord.add("cv`oHgomqC");
                        multiCoord.add("sn|nHcjoqC");
                        multiCoord.add("ss`oHinmqCoA]");
                        linkIntent.putExtra(KEY_COORD, multiCoord);
                        context.startActivity(linkIntent);
                        break;
                    case R.id.btnShareTrip:
                        tripId = trips.get(getAdapterPosition()).getTripId();
                        Intent intent = new Intent(context, ShareTrip.class);
                        intent.putExtra(KEY_TRIP_ID, tripId);
                        context.startActivity(intent);
                        break;
                    case R.id.btnDeleteTrip:
                        tripId = trips.get(getAdapterPosition()).getTripId();
                        trips.remove(getAdapterPosition());
//                        CamnessApiService.deleteTrip(trips.get(getAdapterPosition()).getTripId(), callback);
                        break;
                }
            }
        };

        public TripViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardViewMyTrips);
            tvLink = (TextView) itemView.findViewById(R.id.link);
            tvName = (TextView) itemView.findViewById(R.id.tripName);
            tvDescription = (TextView) itemView.findViewById(R.id.tripDescription);
            tvShare = (TextView) itemView.findViewById(R.id.btnShareTrip);
            tvDelete = (TextView) itemView.findViewById(R.id.btnDeleteTrip);
            tvLink.setOnClickListener(onClickListener);
            tvShare.setOnClickListener(onClickListener);
            tvDelete.setOnClickListener(onClickListener);
        }
    }

    private String generateDynamicLink (String tripId) {
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://example.com/ " + "id = " + tripId))
                .setDynamicLinkDomain("scdf3.app.goo.gl")
                // Open links with this app on Android
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                // Open links with com.example.ios on iOS
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.example.ios").build())

                .buildDynamicLink();

        Uri dynamicLinkUri = dynamicLink.getUri();
        Log.i("dynamicLinkUri", "generateDynamicLink: uri = " + dynamicLink);

        return dynamicLinkUri.toString();
    }
}
