package com.camness.vidimguploadjun2016.trips;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.network.model.TripData;
import com.camness.vidimguploadjun2016.network.response.UploadedFile;

import java.util.ArrayList;
import java.util.List;

public class MyUploadedFilesRecyclerAdapter extends RecyclerView.Adapter<MyUploadedFilesRecyclerAdapter.UploadedFilesViewHolder> {

    private List<UploadedFile> uploadedFiles = new ArrayList<>();
    private TripData tripData = new TripData();


    public MyUploadedFilesRecyclerAdapter(List<UploadedFile> uploadedFiles) {
        this.uploadedFiles = uploadedFiles;
    }

    @Override
    public UploadedFilesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_uploaded_file, parent, false);
        UploadedFilesViewHolder ufvh = new UploadedFilesViewHolder(view);
        return ufvh;
    }

    @Override
    public void onBindViewHolder(UploadedFilesViewHolder holder, final int position) {
        holder.bind(uploadedFiles.get(position));
    }

    @Override
    public int getItemCount() {
        return uploadedFiles.size();
    }

    public TripData getTripData() {
        return tripData;
    }

    class UploadedFilesViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;
        TextView tvAllDescription, tvThumbnail, tvDescription;
        UploadedFile uploadedFile;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadedFile.setChecked(!uploadedFile.isChecked());
                addFilesToUpload(!uploadedFile.isChecked());
            }
        };

        public void bind(UploadedFile uploadedFilesResponse) {
            this.uploadedFile = uploadedFilesResponse;
            tvAllDescription.setText(uploadedFilesResponse.getDate() + " " + uploadedFilesResponse.getTime());
            tvThumbnail.setText(uploadedFilesResponse.getType() + " uploaded by " + uploadedFilesResponse.getUploadedBy());
            tvDescription.setText(uploadedFilesResponse.getDescription());
            checkBox.setChecked(uploadedFilesResponse.isChecked());
        }

        public UploadedFilesViewHolder(View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.chbFile);
            checkBox.setOnClickListener(listener);
            tvAllDescription = (TextView) itemView.findViewById(R.id.tvAllDescription);
            tvThumbnail = (TextView) itemView.findViewById(R.id.tvThumbnail);
            tvDescription = (TextView) itemView.findViewById(R.id.titleDescription);
        }

        private void addFilesToUpload(boolean isChecked) {
            if (isChecked) {
                tripData.removeFileId(uploadedFile.getFileId());
                tripData.removeMultiCoords(uploadedFile.getMultiCoOrdinates());
            } else {
                tripData.addFileId(uploadedFile.getFileId());
                tripData.addFileMultiCoords(uploadedFile.getMultiCoOrdinates());
            }
        }
    }
}
