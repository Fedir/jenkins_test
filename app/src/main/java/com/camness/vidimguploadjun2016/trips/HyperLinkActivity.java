package com.camness.vidimguploadjun2016.trips;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.database.LocalDBManager;
import com.camness.vidimguploadjun2016.network.response.Trips;
import com.camness.vidimguploadjun2016.utils.MapUtility;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;
import java.util.List;

public class HyperLinkActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String KEY_COORD = "coord_key";
    private static final String KEY_TRIP_ID = "keyTripId";
    private static final String TAG = "HyperLinkActivity";
    private List<String> multiCoord;
    private ArrayList<Trips> trips;
    private String tripId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hyper_link);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_link);
        mapFragment.getMapAsync(this);
    }

    private void getData() {
        if (getIntent() != null) {
            multiCoord = getIntent().getStringArrayListExtra(KEY_COORD);
            tripId = getIntent().getStringExtra(KEY_TRIP_ID);
        }

        if (tripId != null) {
            getTrips();
        }

        if (trips != null) {
           getMultiCoord(trips, tripId);
        }
    }

    private void getMultiCoord(ArrayList<Trips> trips, String tripId) {
        for (Trips trip : trips) {
            Log.i(TAG, "tripId = " + tripId + " trips tripId = " + trip.getTripId());
            if (tripId.equals(trip.getTripId())) {
                multiCoord = trip.getMultiCoords();
                Log.i(TAG, "getMultiCoord: " + multiCoord);
            }
        }
    }

    private void getTrips() {
        trips = LocalDBManager.getTrips();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        getData();
        if (multiCoord != null){
            MapUtility.drawRoutes(googleMap, multiCoord, this);
        }
    }
}
