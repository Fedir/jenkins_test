package com.camness.vidimguploadjun2016.infoPin;

import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.detailsRoute.DetailRouteFragment;
import com.camness.vidimguploadjun2016.mapHome.OnMapsFragmentCallback;
import com.camness.vidimguploadjun2016.model.RouteCamnessCluster;
import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.camness.vidimguploadjun2016.utils.Utils;
import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class InfoPinFragment extends Fragment {
    private static String VIDEO_KEY = "video";
    private static String POSITION_KEY = "position";
    private static String COUNT_KEY = "count";
    private final static String HOST_IMAGE = "https://camness.in/images/";
    private final static String HOST_VIDEO = "https://camness.in/video/";
    private final static String ROUTE_KEY = "route";
    private final static String FYLE_TYPE_IMAGE = "image";
    private RouteCamnessCluster routeCamnessCluster;
    private int position;
    private int count;
    private RelativeLayout rootLayout;
    private ImageView closeFragmentIv;
    private ImageView itemThumbnail;
    private OnMapsFragmentCallback onMapsFragmentCallback;
    private TextView counterTv, locationTv, dateTv, categoryTv;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();

            switch (viewId) {
                case R.id.root_layout: {
                    Utils.navigateToFragment(getActivity().getSupportFragmentManager(),
                            R.id.fragment_container,
                            DetailRouteFragment.newInstance(position, routeCamnessCluster),
                            FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                            true);
                }
                break;

                case R.id.close_view_route_iv: {
                    if (onMapsFragmentCallback != null)
                        onMapsFragmentCallback.hideViewPage();
                }
                break;
            }
        }
    };

    public static InfoPinFragment newInstance(RouteCamnessCluster routeCamnessCluster, int position, int count, OnMapsFragmentCallback fragmentListener) {
        InfoPinFragment infoRoute = new InfoPinFragment();
        infoRoute.onMapsFragmentCallback = fragmentListener;
        Bundle bundle = new Bundle();
        bundle.putParcelable(VIDEO_KEY, routeCamnessCluster);
        bundle.putInt(POSITION_KEY, position);
        bundle.putInt(COUNT_KEY, count);
        infoRoute.setArguments(bundle);
        return infoRoute;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_pin_fragment, container, false);
        initUI(view);
        getBundleArguments();
        rootLayout.setOnClickListener(onClickListener);
        closeFragmentIv.setOnClickListener(onClickListener);
        setInfoVideo();
        return view;
    }

    @Override
    public void onDestroyView() {
        onMapsFragmentCallback = null;
        super.onDestroyView();
    }

    private void getBundleArguments() {
        if (getArguments() != null) {
            routeCamnessCluster = getArguments().getParcelable(VIDEO_KEY);
            position = getArguments().getInt(POSITION_KEY);
            count = getArguments().getInt(COUNT_KEY);
        }
    }

    private void setInfoVideo() {
        double lon = Double.parseDouble(routeCamnessCluster.getRouteCamness().getxCord());
        double lng = Double.parseDouble(routeCamnessCluster.getRouteCamness().getyCord());
        RouteCamness routeCamness = routeCamnessCluster.getRouteCamness();
        locationTv.setText(generateAddress(lon, lng));
        dateTv.setText(routeCamnessCluster.getRouteCamness().getDate() + ", " + routeCamnessCluster.getRouteCamness().getTime());
        categoryTv.setText(routeCamnessCluster.getRouteCamness().getCategory());
        counterTv.setText(String.valueOf(position + 1) + " of " + String.valueOf(count));
        if (routeCamnessCluster.getRouteCamness().getFileType().equals(FYLE_TYPE_IMAGE)) {
            Uri uri = Uri.parse(HOST_IMAGE + routeCamness.getFolderName() + routeCamness.getExtension());
            Glide.with(this).load(uri).placeholder(R.drawable.ic_ondemand_video_black_24dp).into(itemThumbnail);
        } else {
            itemThumbnail.setBackgroundResource(R.drawable.ic_play_circle_outline_black_24px);
        }
    }

    private String generateAddress(double lon, double lng) {
        List<Address> addresses = null;
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lon, lng, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder addressBuilder = new StringBuilder();
        if (addresses != null && addresses.size() > 0) {
            if (addresses.get(0).getAddressLine(0) != null) {
                addressBuilder.append(addresses.get(0).getAddressLine(0));
            }

            if (addresses.get(0).getLocality() != null) {
                addressBuilder.append(", ");
                addressBuilder.append(addresses.get(0).getLocality());
            }

            if (addresses.get(0).getAdminArea() != null) {
                addressBuilder.append(", ");
                addressBuilder.append(addresses.get(0).getAdminArea());
            }
        }
        return addressBuilder.toString();
    }

    private void initUI(View view) {
        rootLayout = (RelativeLayout) view.findViewById(R.id.root_layout);
        closeFragmentIv = (ImageView) view.findViewById(R.id.close_view_route_iv);
        itemThumbnail = (ImageView) view.findViewById(R.id.item_thumbnail);
        counterTv = (TextView) view.findViewById(R.id.counter_tv);
        locationTv = (TextView) view.findViewById(R.id.location_start_tv);
        dateTv = (TextView) view.findViewById(R.id.date_start_tv);
        categoryTv = (TextView) view.findViewById(R.id.category_info_pin);
    }
}

