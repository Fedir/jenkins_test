package com.camness.vidimguploadjun2016.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.database.LocalDBManager;
import com.camness.vidimguploadjun2016.enumeration.FileTypeEnum;
import com.camness.vidimguploadjun2016.main.MainActivity;
import com.camness.vidimguploadjun2016.network.CamnessApiService;
import com.camness.vidimguploadjun2016.network.response.UploadResponse;
import com.camness.vidimguploadjun2016.realmModel.RouteCamnessRealm;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadFileService extends Service {
    private static final String KEY_ACTIVITY = "activity";
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private final int ID_UPLOAD_NOTIFICATION = 10;

    private Callback<UploadResponse> uploadResponseCallback = new Callback<UploadResponse>() {
        @Override
        public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
            updateNotification(getString(R.string.upload_complete));
            stopService(new Intent(getApplicationContext(), UploadFileService.class));
        }

        @Override
        public void onFailure(Call<UploadResponse> call, Throwable t) {
            updateNotification(getString(R.string.upload_error));
            stopService(new Intent(getApplicationContext(), UploadFileService.class));
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createNotification();

        uploadFile(intent);

        return super.onStartCommand(intent, flags, startId);
    }


    private void createNotification() {
        notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        notificationBuilder.setContentTitle(getString(R.string.file_upload));
        notificationBuilder.setOngoing(true);
        notificationBuilder.setContentText(getString(R.string.waite_please));
        notificationBuilder.setProgress(0, 0, true);
        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private void updateNotification(String message) {

        Intent intent = new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setContentText(message);
        notificationBuilder.setOngoing(false);
        if (message.equals(getString(R.string.upload_error))) {
            Intent fileReUploadIntent = new Intent(this, UploadFileService.class);
            PendingIntent pendingIntentReLoad = PendingIntent.getService(this, 0, fileReUploadIntent, 0);
            notificationBuilder.setContentIntent(pendingIntentReLoad);
        }
        notificationBuilder.setPriority(NotificationCompat.PRIORITY_MAX);
        if (message.equals(R.string.upload_complete)) {
            notificationBuilder.setContentIntent(pendingIntent);

        }

        notificationManager.notify(ID_UPLOAD_NOTIFICATION, notificationBuilder.build());
    }

    private void showNotification() {
        notificationManager.notify(ID_UPLOAD_NOTIFICATION, notificationBuilder.build());
    }

    private void uploadFile(Intent intentFromApp) {
        if (intentFromApp != null) {
            RouteCamnessRealm routeCamnessRealm = LocalDBManager.loadUploadInfo().getRouteCamnessRealm();
            String userId = LocalDBManager.loadUploadInfo().getUserId();
            String points = LocalDBManager.loadUploadInfo().getPoints();
            String uri = LocalDBManager.loadUploadInfo().getUri();
            //   String uri = "content://media/external/video/media/113778";// alex mobile 3 minut
            FileTypeEnum typeEnum;

            if (LocalDBManager.loadUploadInfo().getFileType().equals("IMAGE")) {
                typeEnum = FileTypeEnum.IMAGE;
            } else if (LocalDBManager.loadUploadInfo().getFileType().equals("VIDEO")) {
                typeEnum = FileTypeEnum.VIDEO;
            } else {
                typeEnum = FileTypeEnum.EMPTY;
            }


            File file = searchFile(typeEnum, uri);
            showNotification();
            if (typeEnum != FileTypeEnum.EMPTY) {

                CamnessApiService.uploadFile(routeCamnessRealm.getVideoDescription(),
                        routeCamnessRealm.getDate(),
                        routeCamnessRealm.getTime(),
                        routeCamnessRealm.getCategory(),
                        routeCamnessRealm.getxCord(),
                        routeCamnessRealm.getyCord(),
                        userId,
                        points,
                        file,
                        uploadResponseCallback);
            } else {
                CamnessApiService.uploadFileWithoutFile(routeCamnessRealm.getVideoDescription(),
                        routeCamnessRealm.getDate(),
                        routeCamnessRealm.getTime(),
                        routeCamnessRealm.getCategory(),
                        routeCamnessRealm.getxCord(),
                        routeCamnessRealm.getyCord(),
                        userId,
                        points,
                        uploadResponseCallback);
            }
        }
    }

    private File searchFile(FileTypeEnum typeEnum, String uri) {
        File file;
        if (typeEnum == FileTypeEnum.VIDEO) {
            file = new File(getRealPathFromURI(Uri.parse(uri)));
            if (!file.exists()) {
                file = new File(Uri.parse(uri).getPath());
            }
        } else {
            file = new File(Uri.parse(uri).getPath());
        }
        return file;
    }

    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            String uriReal = cursor.getString(idx);
            cursor.close();
            return uriReal;
        }
        return "";
    }
}
