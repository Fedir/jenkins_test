package com.camness.vidimguploadjun2016.service;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.MyApplication;
import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.model.Point;
import com.camness.vidimguploadjun2016.utils.Utils;

public class GPSTrackerService extends Service implements LocationListener {
    private static final String TAG = "GPSTrackerService";
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
    private static final long MIN_TIME_BW_UPDATES = 3000;

    private Context mContext;
    private Activity mActivity;
    private Location location;
    private double mLatitude;
    private double mLongitude;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    private LocationManager locationManager;
    private LocationListener locationListener = this;

    public GPSTrackerService() {
    }

    public GPSTrackerService(Activity context) {
        this.mContext = context;
        mActivity = context;
        setEnabled();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.mContext = getApplicationContext();
        MyApplication.getRoutePoints().clear();
        setEnabled();
        startTrackLocation();
        addFirstPoint();
        return super.onStartCommand(intent, flags, startId);
    }

    private void addFirstPoint() {
        Location location = getLastKnownLocation();
        if (location != null) {
            Point point = new Point();
            point.setmLatitude(location.getLatitude());
            point.setmLongitude(location.getLongitude());
            MyApplication.getRoutePoints().add(point);
            Toast.makeText(mContext, "Lat:" + location.getLatitude() + ", Lon:" + location.getLongitude(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        stopUsingGPS();
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(Location location) {
        MyApplication.getRoutePoints().add(new Point(location.getLatitude(), location.getLongitude()));
        Toast.makeText(mContext, "Lat:" + location.getLatitude() + ", Lon:" + location.getLongitude(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public double getmLatitude() {
        if (location != null) {
            mLatitude = location.getLatitude();
        }
        return mLatitude;
    }

    public double getmLongitude() {
        if (location != null) {
            mLongitude = location.getLongitude();
        }
        return mLongitude;
    }

    public Location startTrackLocation() {
        try {
            if (!isGPSEnabled && !isNetworkEnabled) {
//                showSettingsGPSAlert();
            } else {
                this.canGetLocation = true;
                requestLocationUpdate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    public Location getLastKnownLocation() { // get last location
        setEnabled();
        if (isNetworkEnabled || Utils.isNetworkConnected(mContext)) {
            if (locationManager != null) {
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                }
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                }
            }
        } else {
            Toast.makeText(mContext, R.string.no_network_connection, Toast.LENGTH_LONG).show();
            return null;
        }

        if (isGPSEnabled) {
            if (locationManager != null) {
                if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                }
            }
        } else {
    //            showSettingsGPSAlert();
        }
        return location;
    }


    public void stopUsingGPS() {
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.removeUpdates(locationListener);
        }
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    private void requestLocationUpdate() { // track location
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }

        if (isNetworkEnabled) {
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            if (locationManager != null) {
                location = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                }
            }
        }

        if (isGPSEnabled) {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            if (locationManager != null) {
                location = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();
                }
            }
        }
    }

    private void setEnabled() { // set tracking provider
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showSettingsGPSAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle("GPS");
        alertDialog.setMessage("GPS is disabled. Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mActivity.startActivity(intent);

            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }
}