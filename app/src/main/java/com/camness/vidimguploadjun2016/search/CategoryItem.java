package com.camness.vidimguploadjun2016.search;

import java.util.ArrayList;
import java.util.List;

public class CategoryItem {
    private int level;
    private String title;
    private List<CategoryItem> subItems = new ArrayList<>();
    private boolean isExpanded = false;
    private boolean isChecked = false;

    public CategoryItem(int level, String title, List<CategoryItem> subItems) {
        this.level = level;
        this.title = title;
        this.subItems = subItems;
    }

    public boolean hasSubItems() {
        return subItems.size() > 0;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<CategoryItem> getSubItems() {
        return subItems;
    }

    public void setSubItems(List<CategoryItem> subItems) {
        this.subItems = subItems;
    }

    @Override
    public String toString() {
        String res = "{CategoryItem->" + title + " isChecked-" + isChecked;
        if (hasSubItems()) {
            res = res + " Sub->";
            for (CategoryItem item : subItems) {
                res = res + " " + item.toString();
            }
        }
        return res + "}";
    }
}
