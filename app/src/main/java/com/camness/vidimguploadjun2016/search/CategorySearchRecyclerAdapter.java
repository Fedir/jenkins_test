package com.camness.vidimguploadjun2016.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.upload.ListenerNestedScroll;
import com.camness.vidimguploadjun2016.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CategorySearchRecyclerAdapter extends RecyclerView.Adapter<CategorySearchRecyclerAdapter.CategoryHolder> {
    private Context mContext;
    private List<CategoryItem> mDataList;
    private List<CategoryItem> realData;
    private ListenerOpenSubCategory mListenerOpenSubCategory;

    public CategorySearchRecyclerAdapter(Context context, List<CategoryItem> dataItemList, ListenerOpenSubCategory listenerOpenSubCategory) {
        this.mContext = context;
        this.realData = new ArrayList<>(dataItemList);
        this.mDataList = new ArrayList<>();
        this.mListenerOpenSubCategory = listenerOpenSubCategory;
        mDataList.addAll(realData);
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new CategoryHolder(inflater.inflate(R.layout.category_search_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        fillItemCategory(holder, position);
    }

    private void fillItemCategory(CategoryHolder holder, int position) {
        CategoryItem item = mDataList.get(position);
        holder.cb.setChecked(item.isChecked());
        holder.textView.setText(getNameString(item.getTitle(), item.getLevel()));
        if (item.hasSubItems()) {
            holder.cb.setVisibility(View.INVISIBLE);
            holder.arrowDropDownCategorySearch.setVisibility(View.VISIBLE);
        } else {
            holder.cb.setVisibility(View.VISIBLE);
            holder.arrowDropDownCategorySearch.setVisibility(View.GONE);
        }

        if (item.isExpanded()) {
            holder.arrowDropDownCategorySearch.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_drop_up_arrow));
        } else {
            holder.arrowDropDownCategorySearch.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_drop_down_arrow));
        }
    }

    private String getNameString(String title, int level) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level; i++) {
            sb.append("    ");
        }
        sb.append(" ");
        return sb.toString() + title;
    }

    @Override
    public int getItemCount() {
        return mDataList != null ? mDataList.size() : 0;
    }

    public String getSelectedCategories() {
        return getSelected(getZeroLevelItems());
    }

    private List<CategoryItem> getZeroLevelItems() {
        List<CategoryItem> items = new ArrayList<>();
        for (CategoryItem item : mDataList) {
            if (item.getLevel() == 0) items.add(item);
        }
        return items;
    }

    private String getSelected(List<CategoryItem> items) {
        StringBuilder sb = new StringBuilder();
        for (CategoryItem item : items) {
            if (item.hasSubItems()) {
                sb.append(getSelected(item.getSubItems()));
            } else if (item.isChecked()) {
                if (item.getTitle().contains("/")) {
                    String[] singleCategory = item.getTitle().split("/");
                    sb.append(singleCategory[singleCategory.length - 1]);
                } else {
                    sb.append(item.getTitle());
                }
                sb.append(",");
            }
        }
        return sb.toString();
    }

    private void collapseItems(List<CategoryItem> items) {
        Collections.reverse(items);
        List<CategoryItem> reversed = new ArrayList<>(items);
        Collections.reverse(items);
        for (int i = 0; i < items.size(); i++) {
            CategoryItem item = reversed.get(i);
            int index = mDataList.indexOf(item);
            if (item.isExpanded()) {
                collapseItems(item.getSubItems());
                item.setExpanded(false);
            }
            mDataList.remove(index);
            notifyItemRemoved(index);
        }
    }

    private void expandItems(int start, List<CategoryItem> items) {
        for (int i = 0; i < items.size(); i++) {
            mDataList.add(start, items.get(i));
//            mListenerOpenSubCategory.showSubMenu(items);
            notifyItemInserted(start);
            start++;
        }
    }

    class CategoryHolder extends RecyclerView.ViewHolder {
        TextView textView;
        CheckBox cb;
        ImageView arrowDropDownCategorySearch;

        CategoryHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.category_name_search);
            cb = (CheckBox) itemView.findViewById(R.id.category_checkbox_search);
            arrowDropDownCategorySearch = (ImageView) itemView.findViewById(R.id.drop_down_arrow_search);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    makeItemClick(getAdapterPosition(), view);
                }
            });

            cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    makeItemClick(getAdapterPosition(), view);
                }
            });
        }

        private void makeItemClick(int position, View view) {
            Utils.hideKeyboard(mContext, view);
            CategoryItem item = mDataList.get(position);
            if (item.hasSubItems()) {
                mListenerOpenSubCategory.showSubMenu(position);
                if (item.isExpanded()) {
                    collapseItems(item.getSubItems());
                } else {
                    expandItems(position + 1, item.getSubItems());
                }
                item.setExpanded(!item.isExpanded());
                notifyItemRangeChanged(0, mDataList.size());
            } else {
                item.setChecked(!item.isChecked());
                notifyItemChanged(position);
            }
        }
    }

}
