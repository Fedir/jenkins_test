package com.camness.vidimguploadjun2016.search;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.main.OnItemChangeCallback;
import com.camness.vidimguploadjun2016.mapHome.MapsFragment;
import com.camness.vidimguploadjun2016.service.GPSTrackerService;
import com.camness.vidimguploadjun2016.utils.Utils;
import com.camness.vidimguploadjun2016.views.AddressAutoCompleteView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SearchFragment extends Fragment implements DatePickerDialog.OnDateSetListener, ListenerOpenSubCategory {

    private static final String TAG = "SearchFragment";

    private View view;
    private OnItemChangeCallback mFragmentsListener;
    private long dateFrom = 0;
    private long dateTo = 0;
    private ImageView dateFromIv, dateToIv;
    private TextView dateFromTextView, dateToTextView;
    private AddressAutoCompleteView locationEt;
    private Button advancedSearchBtn;
    private DatePickerDialog datePickerDialog;
    private CategorySearchRecyclerAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView mCategoryView;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 5;


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();
            if (viewId == R.id.date_from_iv
                    || viewId == R.id.date_from_text_view || viewId == R.id.date_to_iv || viewId == R.id.date_to_text_view
                    ) {
                showDataPicker(viewId);

            } else if (viewId == R.id.advanced_search_btn) {
                Utils.hideKeyboard(getContext(), getActivity().getCurrentFocus());
                startSearch();
            }
        }
    };

    public static SearchFragment newInstance() {
        Bundle args = new Bundle();
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        initUI();
        setToolbar();
        initCategory();
        dateFromIv.setOnClickListener(onClickListener);
        dateToIv.setOnClickListener(onClickListener);
        dateFromTextView.setOnClickListener(onClickListener);
        dateToTextView.setOnClickListener(onClickListener);
        advancedSearchBtn.setOnClickListener(onClickListener);
        setDefaultData();

        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (Utils.isNetworkConnected(getContext())) {
                getCurrentLocation();
            } else {
                Toast.makeText(getContext(), R.string.no_network_connection, Toast.LENGTH_SHORT).show();
            }
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemChangeCallback) {
            mFragmentsListener = (OnItemChangeCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentsListener = null;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String date = dayOfMonth + "-" + String.format(Locale.US, "%tb", calendar) + "-" + year;
        if ((Integer.parseInt(view.getTag()) == dateFromIv.getId() ||
                (Integer.parseInt(view.getTag()) == dateFromTextView.getId()))) {
            dateFromTextView.setText(date);
            dateFrom = calendar.getTimeInMillis();
        } else {
            dateToTextView.setText(date);
            dateTo = calendar.getTimeInMillis();
        }


    }

    public void setDefaultData() {
        Calendar calendarFrom = Calendar.getInstance();
        Calendar calendarTo = Calendar.getInstance();
//        calendarFrom.set(Calendar.DAY_OF_MONTH, (Calendar.DAY_OF_MONTH - 28));
        String yearFrom = String.valueOf(calendarFrom.get(Calendar.YEAR));
        long time = Calendar.getInstance().getTimeInMillis() - 2592000000L;
        calendarFrom.setTimeInMillis(time);
        String yearTo = String.valueOf(calendarTo.get(Calendar.YEAR));
        String monthOfYearFrom = String.format(Locale.US, "%tb", calendarFrom);
        String monthOfYearTo = String.format(Locale.US, "%tb", calendarTo);
        String dayOfMonth = String.valueOf(calendarFrom.get(Calendar.DAY_OF_MONTH));
        String dayOfMonthTo = String.valueOf(calendarTo.get(Calendar.DAY_OF_MONTH));
        String sDateFrom = dayOfMonth + "-" + monthOfYearFrom + "-" + yearFrom;
        String sDateTo = dayOfMonthTo + "-" + monthOfYearTo + "-" + yearTo;
        dateFrom = calendarFrom.getTimeInMillis();
        dateTo = calendarTo.getTimeInMillis();
        dateFromTextView.setText(sDateFrom);
        dateToTextView.setText(sDateTo);
    }


    private void initCategory() {
        mCategoryView = (RecyclerView) view.findViewById(R.id.category_list);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mCategoryView.setLayoutManager(mLinearLayoutManager);
        mAdapter = new CategorySearchRecyclerAdapter(getActivity(), getCategoryData(), this);
        mCategoryView.setAdapter(mAdapter);
    }

    private List<CategoryItem> getCategoryData() {
        List<CategoryItem> items = new ArrayList<>();
        items.add(getTrafficCategory());
        items.add(getBusinessCategory());
        items.add(getItems(0, "Social", getResources().getStringArray(R.array.Social)));
        return items;
    }

    private CategoryItem getBusinessCategory() {
        List<CategoryItem> items = new ArrayList<>();
        items.add(getItems(1, "WorkShop", new String[]{}));
        items.add(getItems(1, "Hotels", new String[]{}));
        items.add(getItems(1, "Food", new String[]{}));
        items.add(getItems(1, "Gas Station", getResources().getStringArray(R.array.Gas_Station)));
        items.add(getItems(1, "Car Dealership", new String[]{}));
        items.add(getItems(1, "Car Accessories", new String[]{}));
        items.add(getItems(1, "Car Rental", new String[]{}));
        return new CategoryItem(0, "Business", items);
    }

    private CategoryItem getTrafficCategory() {
        List<CategoryItem> items = new ArrayList<>();
        items.add(getItems(1, "Roads", getResources().getStringArray(R.array.Roads)));
        items.add(getItems(1, "Traffic Violation", getResources().getStringArray(R.array.Traffic_Violation)));
        items.add(getItems(1, "Driving", getResources().getStringArray(R.array.Driving)));
        items.add(getItems(1, "Vehicles", getResources().getStringArray(R.array.Vehicles)));
        items.add(getItems(1, "Others-Traffic", new String[]{}));
        return new CategoryItem(0, "Traffic", items);
    }

    private CategoryItem getItems(int level, String title, String[] array) {
        List<CategoryItem> items = new ArrayList<>();
        for (String string : array) {
            items.add(new CategoryItem(level + 1, string, new ArrayList<CategoryItem>()));
        }
        return new CategoryItem(level, title, items);
    }

    private void startSearch() {
        String category = mAdapter.getSelectedCategories();
        if (category.equals("")) {
            Toast.makeText(getActivity(), getString(R.string.typing_category), Toast.LENGTH_SHORT).show();
        } else {
            StringBuilder sb = new StringBuilder(category);
            sb.deleteCharAt(sb.length() - 1);
            Utils.navigateToFragment(getActivity().getSupportFragmentManager(),
                    R.id.fragment_container,
                    MapsFragment.newInstance(locationEt.getText().toString(), sb.toString(), dateFrom, dateTo, true, true),
                    FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                    false);
        }
    }

    private void showDataPicker(int viewId) {
        Calendar calendar = Calendar.getInstance();
        if (viewId == R.id.date_from_text_view || viewId == R.id.date_from_iv) {

            long time = Calendar.getInstance().getTimeInMillis() - 2592000000L;
            calendar.setTimeInMillis(time);


            datePickerDialog = DatePickerDialog.newInstance(
                    this,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)

            );

        } else {
            datePickerDialog = DatePickerDialog.newInstance(
                    this,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            );


        }

        changeLimitDate(viewId);
        datePickerDialog.setAccentColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        datePickerDialog.show(getActivity().getFragmentManager(), "" + viewId);
    }


    private void changeLimitDate(int viewId) {
        if (viewId == R.id.date_from_text_view || viewId == R.id.date_from_iv) {
            Calendar calendarFirst = Calendar.getInstance();
            calendarFirst.set(Calendar.YEAR, 1900);
            datePickerDialog.setMinDate(calendarFirst);

            if (dateTo != 0) {
                Calendar calendarMax = Calendar.getInstance();
                calendarMax.setTimeInMillis(dateTo);
                datePickerDialog.setMaxDate(calendarMax);
            }

        } else {
            Calendar calendarMax = Calendar.getInstance();
            calendarMax.setTimeInMillis(dateTo);
            datePickerDialog.setMaxDate(calendarMax);
            datePickerDialog.setMaxDate(calendarMax);
        }
    }

    private void setToolbar() {
        mFragmentsListener.setTitleToolbar(getString(R.string.search));
        mFragmentsListener.showToolbar();
        mFragmentsListener.enabledSwipeDrawer();
        mFragmentsListener.setDrawerMenu();
        mFragmentsListener.setSelectedNavigationIndex(1);
    }

    private void initUI() {
        dateFromIv = (ImageView) view.findViewById(R.id.date_from_iv);
        dateToIv = (ImageView) view.findViewById(R.id.date_to_iv);
        dateFromTextView = (TextView) view.findViewById(R.id.date_from_text_view);
        dateToTextView = (TextView) view.findViewById(R.id.date_to_text_view);
        advancedSearchBtn = (Button) view.findViewById(R.id.advanced_search_btn);

        locationEt = (AddressAutoCompleteView) view.findViewById(R.id.advanced_search_location_et);
    }

    private void getCurrentLocation() {
        try {
            GPSTrackerService gpsTrackerService = new GPSTrackerService(getActivity());
            Location location = gpsTrackerService.getLastKnownLocation();
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            if (location != null) {
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                locationEt.setText(addresses.get(0).getLocality());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void showSubMenu(int position) {
        mLinearLayoutManager.scrollToPositionWithOffset(position, 0);
    }
}