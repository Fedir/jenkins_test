package com.camness.vidimguploadjun2016.detailsRoute;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;

public class DetailsVideoFragment extends Fragment {
    private final static String HOST = "https://camness.in/videos/";
    private static String ROUTE_KEY = "route";
    private TextView descriptionTv;
    private TextView categoryTv;
    private TextView emptyView;
    private EMVideoView videoView;
    private RouteCamness routeCamness;

    public static DetailsVideoFragment newInstance(RouteCamness routeCamness) {
        DetailsVideoFragment fragment = new DetailsVideoFragment();
        Bundle args = new Bundle();
        args.putParcelable(ROUTE_KEY, routeCamness);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_video, container, false);
        initUI(view);
        if (getArguments() != null) {
            routeCamness = getArguments().getParcelable(ROUTE_KEY);
        }
        setInfo();
        return view;
    }

    private void setInfo() {
        final Uri uri = Uri.parse(HOST + routeCamness.getFolderName() + "/" + routeCamness.getFolderName() + "_" + routeCamness.getExtension());
        videoView.setVideoURI(uri);
        videoView.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion() {
                videoView.setVideoURI(uri);
            }
        });
        videoView.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError() {
                videoView.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
                return false;
            }
        });
        descriptionTv.setText(routeCamness.getVideoDescription());
        categoryTv.setText(routeCamness.getCategory());
    }

    private void initUI(View view) {
        categoryTv = (TextView) view.findViewById(R.id.category_tv);
        descriptionTv = (TextView) view.findViewById(R.id.description_detail_tv);
        videoView = (EMVideoView) view.findViewById(R.id.details_video_view);
        emptyView = (TextView) view.findViewById(R.id.error_view);
    }
}
