package com.camness.vidimguploadjun2016.detailsRoute;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.bumptech.glide.Glide;

public class DetailsImageFragment extends Fragment {
    private final static String HOST = "https://camness.in/images/";
    private static String ROUTE_KEY = "route";
    private TextView descriptionTv;
    private TextView categoryTv;
    private ImageView detailsImage;
    private RouteCamness routeCamness;

    public static DetailsImageFragment newInstance(RouteCamness routeCamness) {
        DetailsImageFragment fragment = new DetailsImageFragment();
        Bundle args = new Bundle();
        args.putParcelable(ROUTE_KEY, routeCamness);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_image, container, false);
        initUI(view);
        if (getArguments() != null) {
            routeCamness = getArguments().getParcelable(ROUTE_KEY);
        }
        setImageInfo();
        return view;
    }

    private void setImageInfo() {
        Uri uri = Uri.parse(HOST + routeCamness.getFolderName() + routeCamness.getExtension());
        Glide.with(this).load(uri.toString()).placeholder(R.drawable.ic_ondemand_video_black_24dp).into(detailsImage);
        descriptionTv.setText(routeCamness.getVideoDescription());
        categoryTv.setText(routeCamness.getCategory());
    }

    private void initUI(View view) {
        categoryTv = (TextView) view.findViewById(R.id.category_tv);
        descriptionTv = (TextView) view.findViewById(R.id.description_detail_tv);
        detailsImage = (ImageView) view.findViewById(R.id.details_image_view);
    }
}
