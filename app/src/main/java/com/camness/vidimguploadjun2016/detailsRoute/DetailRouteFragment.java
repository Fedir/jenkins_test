package com.camness.vidimguploadjun2016.detailsRoute;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.main.OnItemChangeCallback;
import com.camness.vidimguploadjun2016.model.RouteCamnessCluster;
import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.camness.vidimguploadjun2016.utils.ClusterSaver;
import com.camness.vidimguploadjun2016.utils.MapUtility;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DetailRouteFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "DetailRouteFragment";
    private final static String VIDEO_KEY = "video";
    private final static String FILE_TYPE_IMAGE = "image";
    private final static String CURRENT_PAGE = "current_page";
    private OnItemChangeCallback mFragmentsListener;
    private DetailRouteAdapter detailRouteAdapter;
    private ViewPager viewPager;
    private MapView mapView;
    private GoogleMap mMap;
    private View view;
    private ArrayList<RouteCamness> routeCamnessClusters;
    private RouteCamnessCluster routeCamnessCluster;
    private int position;

    public static DetailRouteFragment newInstance(int position, RouteCamnessCluster routeCamnessCluster) {
        Bundle bundle = new Bundle();
        bundle.putInt(CURRENT_PAGE, position);
        bundle.putParcelable(VIDEO_KEY, routeCamnessCluster);
        DetailRouteFragment fragment = new DetailRouteFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail_route, container, false);
        routeCamnessClusters = new ArrayList<>();
        getBundleArguments();
        initMap(savedInstanceState);
        setToolbar();
        initViewPager();
        return view;
    }

    private void getBundleArguments() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            position = bundle.getInt(CURRENT_PAGE, 0);
            routeCamnessCluster = getArguments().getParcelable(VIDEO_KEY);
        }
    }

    private void initMap(Bundle savedInstanceState) {
        mapView = (MapView) view.findViewById(R.id.map_detail);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    private void initViewPager() {
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.getCurrentItem();
        viewPager.addOnPageChangeListener(new PageListener());
        initDetailRouteAdapter();
        setupDetailRouteAdapter();
    }

    private Collection<RouteCamnessCluster> sort(Cluster<RouteCamnessCluster> clickedCluster){
        Collection<RouteCamnessCluster> list = clickedCluster.getItems();
        Collections.sort((List<RouteCamnessCluster>) list, new Comparator<RouteCamnessCluster>() {
            @Override
            public int compare(RouteCamnessCluster o1, RouteCamnessCluster o2) {
                if (o1.getDateTime() < o2.getDateTime())
                    return 1;
                else {
                    return -1;
                }
            }
        });
        return list;
    }

    private void initDetailRouteAdapter() {
        detailRouteAdapter = new DetailRouteAdapter(getActivity().getSupportFragmentManager());
        Cluster<RouteCamnessCluster> clickedCluster = ClusterSaver.getInstance().getClickedCluster();

        if (clickedCluster != null && clickedCluster.getItems().size() > 1) {
            Collection<RouteCamnessCluster> list = sort(clickedCluster);
            for (RouteCamnessCluster camnessCluster : list) {
                if (camnessCluster.getRouteCamness().getFileType().equals(FILE_TYPE_IMAGE)) {
                    addRouteToList(DetailsImageFragment.newInstance(camnessCluster.getRouteCamness()),
                            camnessCluster.getRouteCamness());
                } else {
                    addRouteToList(DetailsVideoFragment.newInstance(camnessCluster.getRouteCamness()),
                            camnessCluster.getRouteCamness());
                }
            }
        } else {
            if (routeCamnessCluster.getRouteCamness().getFileType().equals(FILE_TYPE_IMAGE)) {
                addRouteToList(DetailsImageFragment.newInstance(routeCamnessCluster.getRouteCamness()),
                        routeCamnessCluster.getRouteCamness());
            } else {
                addRouteToList(DetailsVideoFragment.newInstance(routeCamnessCluster.getRouteCamness()),
                        routeCamnessCluster.getRouteCamness());
            }
        }
    }

    private void addRouteToList(Fragment fragment, RouteCamness routeCamness) {
        detailRouteAdapter.addFragment(fragment);
        routeCamnessClusters.add(routeCamness);
    }

    private void setupDetailRouteAdapter() {
        viewPager.setAdapter(detailRouteAdapter);
        viewPager.setOffscreenPageLimit(0);
        viewPager.setSaveEnabled(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemChangeCallback) {
            mFragmentsListener = (OnItemChangeCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentsListener = null;
    }

    private void setToolbar() {
        mFragmentsListener.setArrowToolbar();
        mFragmentsListener.disabledSwipeDrawer();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        moveCameraToInfoPin(routeCamnessClusters.get(position));
        viewPager.setCurrentItem(position);
        mFragmentsListener.setTitleToolbar(routeCamnessClusters.get(position).getTitle());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void moveCameraToInfoPin(RouteCamness routeCamness) {
        if (routeCamness != null) {
            mMap.clear();
            double lon = Double.parseDouble(routeCamness.getxCord());
            double lng = Double.parseDouble(routeCamness.getyCord());
            LatLng point = new LatLng(lon, lng);
            MarkerOptions startMarkerOptions = new MarkerOptions()
                    .position(point)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mMap.addMarker(startMarkerOptions);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 13));
            if (routeCamness.getMultiCoOrdinates() != null) {
                try {
                    MapUtility.drawRoute(mMap, routeCamness.getMultiCoOrdinates(), getActivity());
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Draw route error", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            moveCameraToInfoPin(routeCamnessClusters.get(position));
            mFragmentsListener.setTitleToolbar(routeCamnessClusters.get(position).getTitle());
        }
    }
}
