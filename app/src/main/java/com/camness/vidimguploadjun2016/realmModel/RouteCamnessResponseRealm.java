package com.camness.vidimguploadjun2016.realmModel;


import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.camness.vidimguploadjun2016.network.response.RouteCamnessResponse;
import io.realm.RealmList;
import io.realm.RealmObject;

public class RouteCamnessResponseRealm extends RealmObject {
    public Boolean message;
    public RealmList<RouteCamnessRealm> data = new RealmList<>();
    public Integer statusCode;

    public RouteCamnessResponseRealm() {
    }

    public RouteCamnessResponseRealm(RouteCamnessResponse routeCamnessResponse) {
        setMessage(routeCamnessResponse.getMessage());
        setStatusCode(routeCamnessResponse.getStatusCode());
        for (RouteCamness routeCamness : routeCamnessResponse.getData()) {
            data.add(new RouteCamnessRealm(routeCamness));
        }

    }

    public Boolean getMessage() {
        return message;
    }

    public void setMessage(Boolean message) {
        this.message = message;
    }

    public RealmList<RouteCamnessRealm> getData() {
        return data;
    }

    public void setData(RealmList<RouteCamnessRealm> data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
