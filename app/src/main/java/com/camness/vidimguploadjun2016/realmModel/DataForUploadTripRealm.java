package com.camness.vidimguploadjun2016.realmModel;

import io.realm.RealmList;
import io.realm.RealmObject;


public class DataForUploadTripRealm extends RealmObject {
    private RealmList<String> filesIds = new RealmList<>();
    private RealmList<String> multiCoords = new RealmList<>();
    private String tripName;
    private String tripDescription;


    public DataForUploadTripRealm() {

    }

    public DataForUploadTripRealm(RealmList<String> filesIds, RealmList<String> multiCoords, String tripName, String tripDescription) {
        this.filesIds = filesIds;
        this.multiCoords = multiCoords;
        this.tripName = tripName;
        this.tripDescription = tripDescription;
    }

    public RealmList<String> getFilesIds() {
        return filesIds;
    }

    public void setFilesIds(RealmList<String> filesIds) {
        this.filesIds = filesIds;
    }

    public RealmList<String> getMultiCoords() {
        return multiCoords;
    }

    public void setMultiCoords(RealmList<String> multiCoords) {
        this.multiCoords = multiCoords;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getTripDescription() {
        return tripDescription;
    }

    public void setTripDescription(String tripDescription) {
        this.tripDescription = tripDescription;
    }
}
