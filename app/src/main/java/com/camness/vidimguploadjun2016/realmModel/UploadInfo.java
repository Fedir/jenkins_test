package com.camness.vidimguploadjun2016.realmModel;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UploadInfo extends RealmObject {
    @PrimaryKey
    private long id;
    private RouteCamnessRealm routeCamnessRealm;
    private String uri;
    private String userId;
    private String points;
    private String FileType;

    public UploadInfo() {
    }

    public UploadInfo(RouteCamnessRealm routeCamnessRealm, String uri, String userId, String points, String fileType) {
        this.id = 1;
        this.routeCamnessRealm = routeCamnessRealm;
        this.uri = uri;
        this.userId = userId;
        this.points = points;
        FileType = fileType;
    }

    public RouteCamnessRealm getRouteCamnessRealm() {
        return routeCamnessRealm;
    }

    public String getUri() {
        return uri;
    }

    public String getUserId() {
        return userId;
    }

    public String getPoints() {
        return points;
    }

    public String getFileType() {
        return FileType;
    }
}
