package com.camness.vidimguploadjun2016.realmModel;

import com.camness.vidimguploadjun2016.model.Point;
import com.camness.vidimguploadjun2016.network.response.RouteCamness;

import io.realm.RealmList;
import io.realm.RealmObject;

public class RouteCamnessRealm extends RealmObject {

    private String id;
    private String name;
    private String title;
    private String videoDescription;
    private String xCord;
    private String yCord;
    private String extension;
    private String folderName;
    private String userId;
    private String uploaderName;
    private String category;
    private String views;
    private String likes;
    private String disLikes;
    private String publicOrPrivate;
    private String markerImage;
    private String fileType;
    private String date;
    private String time;
    private String currentPlaylist;
    private String multiCoOrdinates;
    private RealmList<PointRealm> points = new RealmList<>();

    public RouteCamnessRealm() {
    }

    public RouteCamnessRealm(RouteCamness route) {
        setId(route.getId());
        setName(route.getName());
        setTitle(route.getTitle());
        setVideoDescription(route.getVideoDescription());
        setxCord(route.getxCord());
        setyCord(route.getyCord());
        setExtension(route.getExtension());
        setFolderName(route.getFolderName());
        setUserId(route.getUserId());
        setUploaderName(route.getUploaderName());
        setCategory(route.getCategory());
        setViews(route.getViews());
        setLikes(route.getLikes());
        setDisLikes(route.getDisLikes());
        setCurrentPlaylist(route.getCurrentPlaylist());
        setFileType(route.getFileType());
        setDate(route.getDate());
        setTime(route.getTime());
        setPublicOrPrivate(route.getPublicOrPrivate());
        setMarkerImage(route.getMarkerImage());
        setMultiCoOrdinates(route.getMultiCoOrdinates());

        for (Point point : route.getPoints()) {
            points.add(new PointRealm(point));
        }
    }

    public String getMultiCoOrdinates() {
        return multiCoOrdinates;
    }

    public void setMultiCoOrdinates(String multiCoOrdinates) {
        this.multiCoOrdinates = multiCoOrdinates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getxCord() {
        return xCord;
    }

    public void setxCord(String xCord) {
        this.xCord = xCord;
    }

    public String getyCord() {
        return yCord;
    }

    public void setyCord(String yCord) {
        this.yCord = yCord;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUploaderName() {
        return uploaderName;
    }

    public void setUploaderName(String uploaderName) {
        this.uploaderName = uploaderName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDisLikes() {
        return disLikes;
    }

    public void setDisLikes(String disLikes) {
        this.disLikes = disLikes;
    }

    public String getPublicOrPrivate() {
        return publicOrPrivate;
    }

    public void setPublicOrPrivate(String publicOrPrivate) {
        this.publicOrPrivate = publicOrPrivate;
    }

    public String getMarkerImage() {
        return markerImage;
    }

    public void setMarkerImage(String markerImage) {
        this.markerImage = markerImage;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCurrentPlaylist() {
        return currentPlaylist;
    }

    public void setCurrentPlaylist(String currentPlaylist) {
        this.currentPlaylist = currentPlaylist;
    }

    public RealmList<PointRealm> getPoints() {
        return points;
    }

    public void setPoints(RealmList<PointRealm> points) {
        this.points = points;
    }
}
