package com.camness.vidimguploadjun2016.realmModel;

import com.camness.vidimguploadjun2016.network.response.User;

import io.realm.RealmObject;

public class UserRealm extends RealmObject {
    private String name;
    private String id;
    private String lat;
    private String lng;
    private String auth;

    public UserRealm() {
    }

    public UserRealm(User user) {
        setName(user.getName());
        setAuth(user.getAuth());
        setId(user.getId());
        setLat(user.getLat());
        setLng(user.getLng());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
}
