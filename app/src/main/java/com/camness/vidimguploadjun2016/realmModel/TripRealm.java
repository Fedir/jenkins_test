package com.camness.vidimguploadjun2016.realmModel;

import com.camness.vidimguploadjun2016.network.response.Trips;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;


public class TripRealm extends RealmObject {
    private String tripId;
    private String tripName;
    private String tripDescription;
    private String link;
    private RealmList<String> multiCoord = new RealmList<>();
    private boolean isOwn;

    public TripRealm() {

    }

    public TripRealm(Trips trip) {
        setTripId(trip.getTripId());
        setTripName(trip.getTripName());
        setTripDescription(trip.getTripDescription());
        setOwn(trip.isOwn());
        setLink(trip.getLink());
        setMultiCoord((trip.getMultiCoords()));
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getTripDescription() {
        return tripDescription;
    }

    public void setTripDescription(String tripDescription) {
        this.tripDescription = tripDescription;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public void setOwn(boolean own) {
        isOwn = own;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public RealmList<String> getMultiCoord() {
        return multiCoord;
    }

    public void setMultiCoord(List<String> multiCoord) {
        this.multiCoord.addAll(multiCoord);
    }
}
