package com.camness.vidimguploadjun2016.realmModel;

import com.camness.vidimguploadjun2016.model.OverviewPolyline;

import io.realm.RealmObject;

public class OverviewPolylineRealm extends RealmObject {
    String points;

    public OverviewPolylineRealm() {
    }

    public OverviewPolylineRealm(OverviewPolyline points) {
        setPoints(points.getPoints());
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
