package com.camness.vidimguploadjun2016.realmModel;

import com.camness.vidimguploadjun2016.network.response.Trips;
import com.camness.vidimguploadjun2016.network.response.TripsResponse;

import io.realm.RealmList;
import io.realm.RealmObject;


public class TripsResponseRealm extends RealmObject {
    public Boolean message;
    public RealmList<TripRealm> data = new RealmList<>();
    public Integer statusCode;

    public TripsResponseRealm() {
    }

    public TripsResponseRealm(TripsResponse tripsResponse) {
        setMessage(tripsResponse.getMessage());
        setStatusCode(tripsResponse.getStatusCode());
        for (Trips trips : tripsResponse.getData()) {
            data.add(new TripRealm(trips));
        }

    }

    public Boolean getMessage() {
        return message;
    }

    public void setMessage(Boolean message) {
        this.message = message;
    }

    public RealmList<TripRealm> getData() {
        return data;
    }

    public void setData(RealmList<TripRealm> data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
