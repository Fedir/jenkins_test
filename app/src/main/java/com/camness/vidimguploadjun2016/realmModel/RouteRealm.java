package com.camness.vidimguploadjun2016.realmModel;

import com.camness.vidimguploadjun2016.model.Point;
import com.camness.vidimguploadjun2016.model.Route;


import io.realm.RealmList;
import io.realm.RealmObject;

public class RouteRealm extends RealmObject {
    private String videoURL;
    private Long time;
    private RealmList<PointRealm> pointRealms = new RealmList<>();
    private OverviewPolylineRealm overview_polyline = new OverviewPolylineRealm();

    public OverviewPolylineRealm getPolyLine() {
        return overview_polyline;
    }

    public void setPolyLine(OverviewPolylineRealm polyline) {
        this.overview_polyline = polyline;
    }

    public RouteRealm() {
    }

    public RouteRealm(RealmList<PointRealm> pointRealms, Long time) {
        this.pointRealms = pointRealms;
        this.time = time;
    }

    public RouteRealm(Route route) {
        for (Point point : route.getPoints()) {
            pointRealms.add(new PointRealm(point));
        }
        setTime(route.getTime());
        setVideoURL(route.getVideoURL());
        setPolyLine(new OverviewPolylineRealm(route.getPolyLine()));
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public RealmList<PointRealm> getPointRealms() {
        return pointRealms;
    }

    public void setPointRealms(RealmList<PointRealm> pointRealms) {
        this.pointRealms = pointRealms;
    }
}
