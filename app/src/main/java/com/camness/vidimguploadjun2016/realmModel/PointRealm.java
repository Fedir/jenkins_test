package com.camness.vidimguploadjun2016.realmModel;

import com.camness.vidimguploadjun2016.model.Point;

import io.realm.RealmObject;

public class PointRealm extends RealmObject {
    private double mLatitude;
    private double mLongitude;

    public PointRealm() {
    }

    public PointRealm(double mLatitude, double mLongitude) {
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
    }

    public PointRealm(Point point) {
        setmLongitude(point.getmLongitude());
        setmLatitude(point.getmLatitude());
    }

    public double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }
}
