package com.camness.vidimguploadjun2016.mapHome;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.camness.vidimguploadjun2016.MyApplication;
import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.model.Point;
import com.camness.vidimguploadjun2016.service.GPSTrackerService;
import com.camness.vidimguploadjun2016.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;
    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnUpload:
                    setResult(Activity.RESULT_OK);
                    finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tToolbar);
        toolbar.setNavigationIcon(R.drawable.arrow_back_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               pickFromGallery();
            }
        });

        Button btnUpload = (Button) findViewById(R.id.btnUpload);

        btnUpload.setOnClickListener(listener);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setCurrentLocation() {
        GPSTrackerService gps = new GPSTrackerService(this);
        Location location = gps.getLastKnownLocation();
        if (location != null) {
            LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 16)); // move camera to your locationForAdvancedSearch
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                // TODO Auto-generated method stub
                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(point.latitude, point.longitude));
                map.clear();
                map.addMarker(marker);
                Point mPoint = new Point();
                mPoint.setmLatitude(point.latitude);
                mPoint.setmLongitude(point.longitude);
                MyApplication.getRoutePoints().add(mPoint);
            }
        });
        setCurrentLocation();
    }

    private void pickFromGallery() {
        if (!Utils.checkSdPermission(this)) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 0);
    }
}
