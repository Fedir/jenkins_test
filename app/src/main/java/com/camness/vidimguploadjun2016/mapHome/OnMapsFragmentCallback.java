package com.camness.vidimguploadjun2016.mapHome;

public interface OnMapsFragmentCallback {
    void showViewPage();

    void hideViewPage();
}