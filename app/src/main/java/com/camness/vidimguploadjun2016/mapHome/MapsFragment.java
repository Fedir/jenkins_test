package com.camness.vidimguploadjun2016.mapHome;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.CamcorderProfile;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.MyApplication;
import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.database.LocalDBManager;
import com.camness.vidimguploadjun2016.enumeration.FileTypeEnum;
import com.camness.vidimguploadjun2016.infoPin.InfoPinFragment;
import com.camness.vidimguploadjun2016.main.MainActivity;
import com.camness.vidimguploadjun2016.main.OnItemChangeCallback;
import com.camness.vidimguploadjun2016.maps.CustomRenderer;
import com.camness.vidimguploadjun2016.model.RouteCamnessCluster;
import com.camness.vidimguploadjun2016.network.CamnessApiService;
import com.camness.vidimguploadjun2016.network.CancelableCallback;
import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.camness.vidimguploadjun2016.network.response.RouteCamnessResponse;
import com.camness.vidimguploadjun2016.service.GPSTrackerService;
import com.camness.vidimguploadjun2016.upload.UploadFragment;
import com.camness.vidimguploadjun2016.utils.ClusterSaver;
import com.camness.vidimguploadjun2016.utils.PermissionUtils;
import com.camness.vidimguploadjun2016.utils.SharedPreferenceUtil;
import com.camness.vidimguploadjun2016.utils.Utils;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class MapsFragment extends Fragment {
    private static final String LOCATION_SEARCH_KEY = "search_location";
    private static final String CATEGORY_SEARCH_KEY = "search_category";
    private static final String DATE_FROM_SEARCH_KEY = "search_date_from";
    private static final String DATE_TO_SEARCH_KEY = "search_date_to";
    private static final String ALL_CATEGORY = "All";
    private static final String IS_VISIBLE = "is_visible";
    private static final String IS_SEARCH = "is_search";
    private static final int REQUEST_VIDEO_CAPTURE = 2;
    private static final int REQUEST_IMAGE_CAPTURE = 3;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 5;
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 6;
    private static final int PICK_PHOTO_FROM_GALLERY = 0;
    private static final int PICK_VIDEO_FROM_GALLERY = 1;
    private static final int SELECT_PHOTO_LOCATION = 20;
    private static final int SELECT_VIDEO_LOCATION = 25;

    private String category;
    private String locationForAdvancedSearch;
    private long dateFrom = 0;
    private long dateTo = 0;
    private boolean isVisibleRefreshResearch;
    private boolean isSearch;

    private FloatingActionButton cameraVideoFab;
    private FloatingActionButton cameraImageFab;
    private FloatingActionButton emptyRouteFab;
    private FloatingActionsMenu floatingActionsMenu;
    private GoogleMap mMap;
    private MapView mapView;
    private ViewPager mPager;
    private ClusterManager<RouteCamnessCluster> mClusterManager;
    private Cluster<RouteCamnessCluster> clickedCluster;
    private RouteCamnessCluster clickedClusterItem;
    private ViewPagerAdapter mPagerAdapter;
    private Uri imageUri;
    private SearchView searchView;
    private OnItemChangeCallback mFragmentsListener;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private String videoPath;


    private ClusterManager.OnClusterItemClickListener<RouteCamnessCluster> itemClickListener =
            new ClusterManager.OnClusterItemClickListener<RouteCamnessCluster>() {
                @Override
                public boolean onClusterItemClick(RouteCamnessCluster routeCamnessCluster) {
                    showInfoClusterItem(routeCamnessCluster);
                    return false;
                }
            };

    private ClusterManager.OnClusterClickListener<RouteCamnessCluster> clusterClickListener =
            new ClusterManager.OnClusterClickListener<RouteCamnessCluster>() {
                @Override
                public boolean onClusterClick(Cluster<RouteCamnessCluster> cluster) {
                    showInfoCluster(cluster);
                    return false;
                }
            };

    private SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            updateCameraToCurrentCity(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };

    private GoogleMap.OnMapClickListener onMapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            animationDownFab();
            mPager.setVisibility(View.GONE);
            floatingActionsMenu.collapse();
        }
    };

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();

            switch (viewId) {
                case R.id.camera_video_btn: {
                    if (PermissionUtils.checkStoragePermissions(getActivity()) && PermissionUtils.checkLocationPermissions(getActivity())) {
                        showSettingsGPSAlert(getContext(), viewId);
//                        startVideoCamera();
//                        runService();
                    }
                }
                break;

                case R.id.camera_image_btn: {
                    if (PermissionUtils.checkStoragePermissions(getActivity()) && PermissionUtils.checkLocationPermissions(getActivity())) {
                        showSettingsGPSAlert(getContext(), viewId);
//                        startImageCamera();
//                        runService();
                    }
                }
                break;
                case R.id.empty_route_btn: {
                    if (PermissionUtils.checkStoragePermissions(getActivity()) && PermissionUtils.checkLocationPermissions(getActivity())) {
                        showSettingsGPSAlert(getContext(), viewId);

                    }
                }
                break;
            }
        }
    };

    private OnMapsFragmentCallback onMapsFragmentCallback = new OnMapsFragmentCallback() {
        @Override
        public void showViewPage() {
            if (mPager.getVisibility() == View.GONE) {
                mPager.setVisibility(View.VISIBLE);
                animationUpFab();
                animationUpViewPager();
            }
        }

        @Override
        public void hideViewPage() {
            mPager.setVisibility(View.GONE);
            animationDownFab();
            animationDownViewPager();
        }
    };

    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
            setupMap();
        }
    };

    public static MapsFragment newInstance() {
        return new MapsFragment();
    }

    public static MapsFragment newInstance(String location, String category, long dateFrom, long dateTo, boolean isVisibleRefresh, boolean isSearch) {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putString(LOCATION_SEARCH_KEY, location);
        args.putString(CATEGORY_SEARCH_KEY, category);
        args.putLong(DATE_FROM_SEARCH_KEY, dateFrom);
        args.putLong(DATE_TO_SEARCH_KEY, dateTo);
        args.putBoolean(IS_VISIBLE, isVisibleRefresh);
        args.putBoolean(IS_SEARCH, isSearch);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        initUI(view);
        initMapView(savedInstanceState);
        getBundleArguments();
        setToolbar();
        checkLogin();
        ClusterSaver.getInstance().setClickedCluster(null);
        sharedPreferenceUtil = new SharedPreferenceUtil(getContext());
        return view;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        sharedPreferenceUtil.deleteSavedPosition();
        super.onDestroy();
        closeService();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        closeService();
        super.onDestroyView();
    }


    @Override
    public void onPause() {
        super.onPause();
        sharedPreferenceUtil.saveCameraPosition(mMap);
        mapView.onPause();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnItemChangeCallback) {
            mFragmentsListener = (OnItemChangeCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        sharedPreferenceUtil.deleteSavedPosition();
        mFragmentsListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        initMenuItem(menu);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        closeService();
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
                if (data.getData() != null) {
                    startUploadFragment(data.getData().toString(), FileTypeEnum.VIDEO);
                }
            }
            if (requestCode == PICK_VIDEO_FROM_GALLERY && resultCode == RESULT_OK) {
                videoPath = data.getData().toString();
                startActivityForResult(new Intent(getContext(), MapActivity.class), SELECT_VIDEO_LOCATION);
            }
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                if (imageUri != null) {
                    startUploadFragment(getRealPathFromURI(imageUri), FileTypeEnum.IMAGE);
                }
            }
            if (requestCode == PICK_PHOTO_FROM_GALLERY && resultCode == RESULT_OK) {
                imageUri = data.getData();
                startActivityForResult(new Intent(getContext(), MapActivity.class), SELECT_PHOTO_LOCATION);
            }
            if (requestCode == SELECT_PHOTO_LOCATION && resultCode == RESULT_OK) {
                if (imageUri != null) {
                    startUploadFragment(getRealPathFromURI(imageUri), FileTypeEnum.IMAGE);
                }
            }
            if (requestCode == SELECT_VIDEO_LOCATION && resultCode == RESULT_OK) {
                startUploadFragment(videoPath, FileTypeEnum.VIDEO);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            confirmLocationPermission(grantResults);
        } else if (requestCode == MY_PERMISSIONS_REQUEST_STORAGE) {
            confirmStoragePermission(grantResults);
        }
    }

    private void setupMap() {
        mMap.getUiSettings().setMapToolbarEnabled(false);
        if (PermissionUtils.checkLocationPermissions(getActivity())) {
            setCameraLocation(sharedPreferenceUtil.isSavedPositionExist());
        }
        mMap.setOnMapClickListener(onMapClickListener);
        if (isSearch) {
            getSearchPins();
        } else {
            getAllPins();
        }
    }

    private ArrayList<RouteCamnessCluster> sort() {
        ArrayList<RouteCamnessCluster> arrayList = new ArrayList<>(clickedCluster.getItems());
        Collections.sort(arrayList, new Comparator<RouteCamnessCluster>() {
            @Override
            public int compare(RouteCamnessCluster o1, RouteCamnessCluster o2) {
                long first = o1.getRouteCamness().getDateTime();
                long second = o2.getRouteCamness().getDateTime();
                if (first <= second) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

        return arrayList;
    }

    private void addFragmentToViewPager(Cluster<RouteCamnessCluster> cluster) {
        clickedCluster = cluster;
        ClusterSaver.getInstance().setClickedCluster(clickedCluster);
        mPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        int i = 0;
        int size = clickedCluster.getItems().size();
        ArrayList<RouteCamnessCluster> arrayList = sort();

        for (RouteCamnessCluster oneCluster : arrayList) {
            mPagerAdapter.addFragment(InfoPinFragment.newInstance(oneCluster, i, size, onMapsFragmentCallback));
            i++;
        }
    }

    private void setCameraLocation(boolean isSavedPositionExist) {
        if (!isSavedPositionExist) {
            if (locationForAdvancedSearch == null || locationForAdvancedSearch.equals("")) {
                setCurrentLocation();
            } else {
                updateCameraToCurrentCity(locationForAdvancedSearch);
            }
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            }
            mMap.setMyLocationEnabled(true);
        } else {
            mMap.animateCamera(sharedPreferenceUtil.getSavedCameraPosition());
            mMap.setMyLocationEnabled(true);
            sharedPreferenceUtil.deleteSavedPosition();
        }
    }

    private void getBundleArguments() {
        if (getArguments() != null) {
            category = getArguments().getString(CATEGORY_SEARCH_KEY);
            locationForAdvancedSearch = getArguments().getString(LOCATION_SEARCH_KEY);
            dateFrom = getArguments().getLong(DATE_FROM_SEARCH_KEY);
            dateTo = getArguments().getLong(DATE_TO_SEARCH_KEY);
            isVisibleRefreshResearch = getArguments().getBoolean(IS_VISIBLE);
            isSearch = getArguments().getBoolean(IS_SEARCH);
        } else {
            category = ALL_CATEGORY;
            locationForAdvancedSearch = null;
        }
    }

    private void confirmLocationPermission(int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            setCurrentLocation();
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    private void confirmStoragePermission(int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            setCurrentLocation();
        }
    }

    private void showInfoClusterItem(RouteCamnessCluster routeCamnessCluster) {
        clickedClusterItem = routeCamnessCluster;
        mPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        mPagerAdapter.addFragment(InfoPinFragment.newInstance(clickedClusterItem, 0, 1, onMapsFragmentCallback));
        setAdapter();
        onMapsFragmentCallback.showViewPage();
    }

    private void showInfoCluster(Cluster<RouteCamnessCluster> cluster) {
        addFragmentToViewPager(cluster);
        setAdapter();
        animationUpFab();
        onMapsFragmentCallback.showViewPage();
    }

    private void initMenuItem(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.action_search);
        MenuItem refreshItem = menu.findItem(R.id.action_refresh_search);
        refreshItem.setVisible(isVisibleRefreshResearch);
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            searchItem.setVisible(true);
        }
        if (searchView != null) {
            searchView.setOnQueryTextListener(onQueryTextListener);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh_search) {
            MainActivity.start(getActivity());
        }
        return super.onOptionsItemSelected(item);
    }

    private void getAllPins() {

        CamnessApiService.getVideo(category, new CancelableCallback() {
            @Override
            public void onResponse(Call call, Response response) {
                final RouteCamnessResponse routeCamnessResponse = (RouteCamnessResponse) response.body();
                if (isVisible() && routeCamnessResponse != null) {
                    initClusturer(routeCamnessResponse);
                    LocalDBManager.deleteRoutCamnessResponse();
                    LocalDBManager.saveRoutCamnessResponse(routeCamnessResponse);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (isVisible()) {
                    if (!Utils.isNetworkConnected(getContext())) {
                        Toast.makeText(getContext(), R.string.no_network_connection, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.no_video, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void getSearchPins() {
        String latLng = Utils.getLocationFromAddress(getActivity(), locationForAdvancedSearch);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateStart = formatter.format(new Date(dateFrom));
        String dateEnd = formatter.format(new Date(dateTo));
        CamnessApiService.getVideoByCategory(100, latLng, category, dateStart, dateEnd, new CancelableCallback() {
            @Override
            public void onResponse(Call call, Response response) {
                final RouteCamnessResponse routeCamnessResponse = (RouteCamnessResponse) response.body();
                if (isVisible() && routeCamnessResponse != null) {
                    initClusturer(routeCamnessResponse);
                    LocalDBManager.deleteRoutCamnessResponse();
                    LocalDBManager.saveRoutCamnessResponse(routeCamnessResponse);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (isVisible()) {
                    if (!Utils.isNetworkConnected(getContext())) {
                        Toast.makeText(getContext(), R.string.no_network_connection, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.no_video, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void setToolbar() {
        mFragmentsListener.showToolbar();
        if (getArguments() == null) {
            mFragmentsListener.setTitleToolbar(getString(R.string.map_title));
        } else {
            mFragmentsListener.setTitleToolbar(category);
        }
        mFragmentsListener.setDrawerMenu();
        mFragmentsListener.enabledSwipeDrawer();
        mFragmentsListener.setSelectedNavigationIndex(0);
    }

    private void initMapView(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(onMapReadyCallback);
    }

    private void startUploadFragment(String data, FileTypeEnum fileType) {
        RouteCamness route = new RouteCamness();
        route.setPoints(new ArrayList<>(MyApplication.getRoutePoints()));
        if (route.getPoints().size() > 0) {
            route = initRoute(route);
            Utils.navigateToFragment(getActivity().getSupportFragmentManager(),
                    R.id.fragment_container,
                    UploadFragment.newInstance(route, data, fileType),
                    FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                    true);
        }
    }

    private void runService() {
        getActivity().startService(new Intent(getContext(), GPSTrackerService.class)); // start tracker
    }

    private void closeService() {
        getActivity().stopService(new Intent(getContext(), GPSTrackerService.class));
    }

    private void startVideoCamera() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE).putExtra(String.valueOf(CamcorderProfile.QUALITY_HIGH), 720);
        if (takeVideoIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
        floatingActionsMenu.collapse();
    }

    private void startImageCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, getString(R.string.new_picture));
        values.put(MediaStore.Images.Media.DESCRIPTION, getString(R.string.from_camera));
        imageUri = getContext().getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        floatingActionsMenu.collapse();
    }

    private void setCurrentLocation() {
        GPSTrackerService gps = new GPSTrackerService(getActivity());
        Location location = gps.getLastKnownLocation();
        if (location != null) {
            LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 16)); // move camera to your locationForAdvancedSearch
        }
    }

    private void updateCameraToCurrentCity(String location) {
        List<Address> addresses;
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        final int MAX_RESULTS = 1;
        final int SEARCHED_ADDRESS = 0;
        try {
            addresses = geocoder.getFromLocationName(location, MAX_RESULTS);
            if (addresses.size() > 0) {
                LatLng latLng = new LatLng(addresses.get(SEARCHED_ADDRESS).getLatitude(), addresses.get(SEARCHED_ADDRESS).getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initUI(View view) {
        mapView = (MapView) view.findViewById(R.id.map_main);
        cameraVideoFab = (FloatingActionButton) view.findViewById(R.id.camera_video_btn);
        cameraImageFab = (FloatingActionButton) view.findViewById(R.id.camera_image_btn);
        emptyRouteFab = (FloatingActionButton) view.findViewById(R.id.empty_route_btn);
        mPager = (ViewPager) view.findViewById(R.id.pager);
        floatingActionsMenu = (FloatingActionsMenu) view.findViewById(R.id.multiple_actions_up);
        mPager.setClipToPadding(false);
        cameraVideoFab.setOnClickListener(onClickListener);
        cameraImageFab.setOnClickListener(onClickListener);
        emptyRouteFab.setOnClickListener(onClickListener);
        floatingActionsMenu.setOnClickListener(onClickListener);
    }

    private void initClusturer(RouteCamnessResponse response) {
        setClusterManager();

        addMapCluster(response.getData());

//        List<RouteCamness> list = sort(response);
        mClusterManager.setOnClusterClickListener(clusterClickListener);
        mClusterManager.setOnClusterItemClickListener(itemClickListener);
    }

    private void animationUpFab() {
        float distance = getResources().getDimensionPixelSize(R.dimen.distanceUp);
        floatingActionsMenu.animate().translationY(distance).setDuration(400).start();
    }

    private void animationDownFab() {
        float distance = getResources().getDimensionPixelSize(R.dimen.distanceDown);
        floatingActionsMenu.animate().translationY(distance).setDuration(400).start();
    }

    private void animationUpViewPager() {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.move_up_view_pager);
        mPager.startAnimation(animation);
    }

    private void animationDownViewPager() {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.move_down_view_pager);
        mPager.startAnimation(animation);
    }

    private void addMapCluster(List<RouteCamness> list) {
        if (dateTo != 0 && dateFrom != 0) {
            Calendar calendarFrom = Calendar.getInstance();
            calendarFrom.setTimeInMillis(dateFrom);
            Calendar calendarTo = Calendar.getInstance();
            calendarTo.setTimeInMillis(dateTo);
            for (int i = 0; i < list.size(); i++) {
                RouteCamnessCluster routeCamnessCluster = new RouteCamnessCluster(list.get(i));
                if (!routeCamnessCluster.getRouteCamness().getxCord().equals("")) {
                    mClusterManager.addItem(routeCamnessCluster);
                }
            }
            dateTo = dateFrom = 0;
        } else {
            for (int i = 0; i < list.size(); i++) {
                RouteCamnessCluster routeCamnessCluster = new RouteCamnessCluster(list.get(i));
                if (!routeCamnessCluster.getRouteCamness().getxCord().equals("")) {
                    mClusterManager.addItem(routeCamnessCluster);
                }
            }
        }
        mClusterManager.cluster();
    }

    private void setClusterManager() {
        mClusterManager = new ClusterManager<>(getContext(), mMap);
        mClusterManager.setRenderer(new CustomRenderer<RouteCamnessResponse>(getContext(), mMap, mClusterManager));
        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
    }

    private void setAdapter() {
        mPager.setAdapter(mPagerAdapter);
        mPager.setOffscreenPageLimit(2);
        mPager.setSaveEnabled(false);
    }

    private RouteCamness initRoute(RouteCamness route) {
        Calendar calendar = Calendar.getInstance();
        String time = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
        String date = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);

        route.setTime(time);
        route.setDate(date);
        route.setxCord(String.valueOf(route.getPoints().get(route.getPoints().size() - 1).getmLatitude()));
        route.setyCord(String.valueOf(route.getPoints().get(route.getPoints().size() - 1).getmLongitude()));
        return route;
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        if (cursor.getString(column_index) != null) {
            return cursor.getString(column_index);
        } else {
            getRealPathFromURI(contentUri);
            return cursor.getString(column_index);
        }

    }

    private void checkLogin() {
        updateUIForUser(LocalDBManager.userIsLoggedIn());
    }

    private void updateUIForUser(boolean isLoggedIn) {
        if (isLoggedIn) {
            floatingActionsMenu.setVisibility(View.VISIBLE);
            mFragmentsListener.setLoginUserUI();
        } else {
            floatingActionsMenu.setVisibility(View.GONE);
            mFragmentsListener.setLogoutUserUI();
        }
    }

    private void selectVideo() {
        final String[] items = {getString(R.string.record_video), getString(R.string.from_gallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.record_video))) {
                    startVideoCamera();
                    runService();
                } else if (items[item].equals(getString(R.string.from_gallery))) {
                    pickVideoFromGallery();
                }
            }
        });
        builder.show();
    }

    private void selectImage() {
        final String[] items = {getString(R.string.make_photo), getString(R.string.from_gallery)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.make_photo))) {
                    startImageCamera();
                    runService();
                } else if (items[item].equals(getString(R.string.from_gallery))) {
                    pickFromGallery();
                }
            }
        });
        builder.show();
    }

    private void pickVideoFromGallery() {
        if (!Utils.checkSdPermission(getActivity())) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 1);
    }

    private void pickFromGallery() {
        if (!Utils.checkSdPermission(getActivity())) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_PHOTO_FROM_GALLERY);
    }

    public void showSettingsGPSAlert(Context context, int viewId) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Ask the user to enable GPS
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Location Manager");
            builder.setMessage("Would you like to enable GPS?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Launch settings, allowing user to make a change
                    Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(i);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //No location service, no Activity
                    dialog.dismiss();
                }
            });
            builder.create().show();
        } else if (viewId == R.id.camera_video_btn) {

            selectVideo();
        } else if (viewId == R.id.camera_image_btn) {
            selectImage();
        } else if (viewId == R.id.empty_route_btn) {
            runService();
            Utils.navigateToFragment(getActivity().getSupportFragmentManager(),
                    R.id.fragment_container,
                    UploadFragment.newInstance(new RouteCamness(), "", FileTypeEnum.EMPTY),
                    FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                    true);

        }
    }
}
