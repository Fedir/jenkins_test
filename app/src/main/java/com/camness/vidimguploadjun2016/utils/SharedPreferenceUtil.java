package com.camness.vidimguploadjun2016.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class SharedPreferenceUtil {

    private static final String SAVE_CAMERA_POSITION = "SAVE_CAMERA_POSITION";
    private static final String CAMERA_LONGITUDE = "CAMERA_LONGITUDE";
    private static final String CAMERA_LATITUDE = "CAMERA_LATITUDE";
    private static final String CAMERA_ZOOM = "CAMERA_ZOOM";
    private  Context context;

    public SharedPreferenceUtil(Context context){
        this.context = context;
    }

    public void saveCameraPosition(GoogleMap mMap){
        if (mMap != null) {
            CameraPosition mMyCam = mMap.getCameraPosition();
            double longitude = mMyCam.target.longitude;
            double latitude = mMyCam.target.latitude;
            float zoom = mMyCam.zoom;

            SharedPreferences settings = context.getSharedPreferences(SAVE_CAMERA_POSITION, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putFloat(CAMERA_LONGITUDE, (float) longitude);
            editor.putFloat(CAMERA_LATITUDE, (float) latitude);
            editor.putFloat(CAMERA_ZOOM, zoom);
            editor.apply();
        }
    }

    public CameraUpdate getSavedCameraPosition(){
        SharedPreferences settings = context.getSharedPreferences(SAVE_CAMERA_POSITION, 0);

        double longitude = settings.getFloat(CAMERA_LONGITUDE, -1);
        double latitude = settings.getFloat(CAMERA_LATITUDE, -1);
        float zoom = settings.getFloat(CAMERA_ZOOM, -1);

        if(longitude != -1 && latitude != -1 && zoom != -1) {
            return CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), zoom);
        } else return null;
    }

    public boolean isSavedPositionExist(){
        CameraUpdate cameraUpdate = getSavedCameraPosition();
        if(cameraUpdate != null)
            return true;
        else return false;
    }

    public void deleteSavedPosition(){
        SharedPreferences preferences = context.getSharedPreferences(SAVE_CAMERA_POSITION, 0);
        preferences.edit().remove(CAMERA_LONGITUDE)
                .remove(CAMERA_LATITUDE)
                .remove(CAMERA_ZOOM)
                .apply();
    }
}
