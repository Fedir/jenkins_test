package com.camness.vidimguploadjun2016.utils;

import android.content.Context;

import com.camness.vidimguploadjun2016.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.List;

public class MapUtility {
    public static GoogleMap drawRoute(GoogleMap map, String points, Context context) throws StringIndexOutOfBoundsException {

/*        points.add(new Point(40.692651, -73.958698 ));
        points.add(new Point(40.695048, -73.962569 ));
        points.add(new Point(40.697641, -73.954888));
        points.add(new Point(40.698934, -73.955103));
        points.add(new Point(40.699829, -73.953644));
        points.add(new Point(40.699772, -73.956712));
        points.add(new Point(40.699118, -73.956209));*/
        //  List<LatLng> mPoints = new ArrayList<>();

      /*  for (Point po : points) {
            mPoints.add(new LatLng(po.getmLatitude(), po.getmLongitude()));
        }*/


        List<LatLng> mPoints = PolyUtil.decode(points);
        PolylineOptions line = new PolylineOptions();
        line.width(15f).color(R.color.colorPrimaryDark);
        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
        for (int i = 0; i < mPoints.size(); i++) {
            if (i == 0) { // draw marker for first and last point.
                MarkerOptions startMarkerOptions = new MarkerOptions()
                        .position(mPoints.get(i))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                map.addMarker(startMarkerOptions);
            } else if (i == mPoints.size() - 1 && !compareMainPoints(mPoints)) {
                MarkerOptions endMarkerOptions = new MarkerOptions()
                        .position(mPoints.get(i))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                map.addMarker(endMarkerOptions);
            }
            line.add(mPoints.get(i));
            latLngBuilder.include(mPoints.get(i));
        }

        map.addPolyline(line);
        int size = context.getResources().getDisplayMetrics().widthPixels;
        LatLngBounds latLngBounds = latLngBuilder.build();
        CameraUpdate track = CameraUpdateFactory.newLatLngBounds(latLngBounds, size, size, 15);
        map.moveCamera(track);
        return map;
    }

    private static boolean compareMainPoints(List<LatLng> mPoints) {
        return mPoints.get(0).latitude == mPoints.get(mPoints.size() - 1).latitude
                && mPoints.get(0).longitude == mPoints.get(mPoints.size() - 1).longitude;

    }

    public static GoogleMap drawRoutes(GoogleMap map, List<String> points, Context context) throws StringIndexOutOfBoundsException {

        for (String point : points){
            List<LatLng> mPoints = PolyUtil.decode(point);
            PolylineOptions line = new PolylineOptions();
            line.width(15f).color(R.color.colorPrimaryDark);
            LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
            for (int i = 0; i < mPoints.size(); i++) {
                if (i == 0) { // draw marker for first and last point.
                    MarkerOptions startMarkerOptions = new MarkerOptions()
                            .position(mPoints.get(i))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    map.addMarker(startMarkerOptions);
                } else if (i == mPoints.size() - 1 && !compareMainPoints(mPoints)) {
                    MarkerOptions endMarkerOptions = new MarkerOptions()
                            .position(mPoints.get(i))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    map.addMarker(endMarkerOptions);
                }
                line.add(mPoints.get(i));
                latLngBuilder.include(mPoints.get(i));
            }

            map.addPolyline(line);
            int size = context.getResources().getDisplayMetrics().widthPixels;
            LatLngBounds latLngBounds = latLngBuilder.build();
            CameraUpdate track = CameraUpdateFactory.newLatLngBounds(latLngBounds, size, size, 15);
            map.moveCamera(track);
        }
        return map;
    }
}
