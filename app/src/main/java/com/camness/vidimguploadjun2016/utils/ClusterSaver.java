package com.camness.vidimguploadjun2016.utils;

import com.camness.vidimguploadjun2016.model.RouteCamnessCluster;
import com.google.maps.android.clustering.Cluster;

public class ClusterSaver {
    private static ClusterSaver ourInstance = new ClusterSaver();
    private Cluster<RouteCamnessCluster> clickedCluster;

    public static ClusterSaver getInstance() {
        return ourInstance;
    }

    private ClusterSaver() {}

    public Cluster<RouteCamnessCluster> getClickedCluster() {
        return clickedCluster;
    }

    public void setClickedCluster(Cluster<RouteCamnessCluster> clickedCluster) {
        this.clickedCluster = clickedCluster;
    }
}
