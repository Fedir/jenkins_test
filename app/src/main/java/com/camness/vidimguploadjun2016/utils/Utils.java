package com.camness.vidimguploadjun2016.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.camness.vidimguploadjun2016.network.response.RouteCamness;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Utils {

    public static Fragment lastFragment;
    private static final int REQUEST_PICK = 213;


    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void navigateToFragment(FragmentManager fragmentManager,
                                          int container,
                                          Fragment fragment,
                                          int transaction,
                                          boolean backStack) {
        lastFragment = fragment;
        if (backStack) {
            fragmentManager.beginTransaction()
                    .setTransition(transaction)
                    .replace(container, fragment)
                    .addToBackStack(null)
                    .commit();

        } else {
            fragmentManager.beginTransaction()
                    .setTransition(transaction)
                    .replace(container, fragment)
                    .commit();
        }

    }

    public static void hideKeyboard(Context context, View field) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(field.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context, View field) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.showSoftInputFromInputMethod(field.getWindowToken(), 0);
    }

    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static int comparisonTwoDate(Calendar calendarFirst, Calendar calendarSecond) { // firt>second return 1; <return -1
        if (calendarFirst.get(Calendar.YEAR) == calendarSecond.get(Calendar.YEAR)
                && calendarFirst.get(Calendar.MONTH) == calendarSecond.get(Calendar.MONTH)
                && calendarFirst.get(Calendar.DAY_OF_MONTH) == calendarSecond.get(Calendar.DAY_OF_MONTH)) {
            return 0;
        } else if (calendarFirst.get(Calendar.YEAR) > calendarSecond.get(Calendar.YEAR)) {
            return 1;

        } else if (calendarFirst.get(Calendar.MONTH) > calendarSecond.get(Calendar.MONTH)) {
            return 1;

        } else if (calendarFirst.get(Calendar.DAY_OF_MONTH) > calendarSecond.get(Calendar.DAY_OF_MONTH)) {
            return 1;
        }

        return -1;
    }

    public static Calendar convertStringToDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }
    public static RouteCamness initRoute(RouteCamness route) {
        Calendar calendar = Calendar.getInstance();
        String time = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
        String date = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
        route.setTime(time);
        route.setDate(date);
        route.setxCord(String.valueOf(route.getPoints().get(0).getmLatitude()));
        route.setyCord(String.valueOf(route.getPoints().get(0).getmLongitude()));
        return route;
    }
    public static String getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;

        double lat = 0;
        double lng = 0;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            lat = location.getLatitude();
            lng = location.getLongitude();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return lat + "," + lng;
    }

    public static boolean checkSdPermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean read = activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
            boolean write = activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
            if (!read || !write) {
               activity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PICK);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }
}
