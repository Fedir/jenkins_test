package com.camness.vidimguploadjun2016.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.database.LocalDBManager;
import com.camness.vidimguploadjun2016.detailsRoute.DetailRouteFragment;
import com.camness.vidimguploadjun2016.login.LoginFragment;
import com.camness.vidimguploadjun2016.mapHome.MapsFragment;
import com.camness.vidimguploadjun2016.network.response.User;
import com.camness.vidimguploadjun2016.registration.RegisterFragment;
import com.camness.vidimguploadjun2016.resetPassword.ResetPasswordFragment;
import com.camness.vidimguploadjun2016.search.SearchFragment;
import com.camness.vidimguploadjun2016.trips.CreateTrip;
import com.camness.vidimguploadjun2016.trips.HyperLinkActivity;
import com.camness.vidimguploadjun2016.trips.TripsFragment;
import com.camness.vidimguploadjun2016.upload.UploadFragment;
import com.camness.vidimguploadjun2016.utils.Utils;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

public class MainActivity extends AppCompatActivity implements OnItemChangeCallback, GoogleApiClient.OnConnectionFailedListener {


    private static final int PRESS_AGAIN_TIME = 2000;

    private Toolbar mToolbar;
    private View header;
    private TextView usernameHeader;
    private NavigationView navigationView;
    private DrawerLayout mDrawer;
    private User user;
    private static final int NAVIGATION_HEADER_ITEM = 0;
    private static final int NAVIGATION_LOGIN_ITEM = 3;
    private static final int NAVIGATION_LOGOUT_ITEM = 4;
    private static final String KEY_TRIP_ID = "keyTripId";

    private boolean isBackPressed;
    private long pressedTime;

    private DialogInterface.OnClickListener dialogCancelClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
    };

    private DialogInterface.OnClickListener dialogSaveClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            LocalDBManager.deleteUserFromDB();
            setLogoutUserUI();
            navigateToFragment(MapsFragment.newInstance(), false);
        }
    };

    private NavigationView.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Utils.navigateToFragment(getSupportFragmentManager(),
                    R.id.fragment_container,
                    MapsFragment.newInstance(),
                    FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                    false);
        }
    };

    private NavigationView.OnNavigationItemSelectedListener selectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Utils.hideKeyboard(MainActivity.this, getCurrentFocus());
            int id = item.getItemId();
            Fragment fr = getSupportFragmentManager().findFragmentById(R.id.fragment_container);

            if (id == R.id.map_nav) {
                if (!(fr instanceof MapsFragment)) {
                    navigateToFragment(MapsFragment.newInstance(), false);
                }

            } else if (id == R.id.log_in_nav) {
                if (!(fr instanceof LoginFragment)) {
                    navigateToFragment(LoginFragment.newInstance(), false);
                }

            } else if (id == R.id.log_out_nav) {
                showLogOutAlert();

            } else if (id == R.id.advanced_search_nav) {
                if (!(fr instanceof SearchFragment)) {
                    navigateToFragment(SearchFragment.newInstance(), false);
                }
            } else if (id == R.id.trips_nav) {
                if (!(fr instanceof TripsFragment)) {
                    navigateToFragment(TripsFragment.newInstance(), false);
                }
            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }
    };

    public static void start(Context context) {
        Intent starter = new Intent(context, MainActivity.class);
        starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(starter);
    }

    @Override
    @SuppressWarnings("RestrictedApi")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        setToolbar();
        setDrawerMenu();
        checkDeepLink();
        navigationView.setNavigationItemSelectedListener(selectedListener);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (Utils.lastFragment != null && (Utils.lastFragment instanceof DetailRouteFragment
                    || Utils.lastFragment instanceof UploadFragment
                    || Utils.lastFragment instanceof RegisterFragment
                    || Utils.lastFragment instanceof ResetPasswordFragment)) {
                super.onBackPressed();
                Utils.lastFragment = null;
                return;
            }
            if (!isBackPressed) {
                isBackPressed = true;
                pressedTime = System.currentTimeMillis();
                Toast.makeText(this, R.string.press_again_to_exit, Toast.LENGTH_SHORT).show();
            } else {
                if (System.currentTimeMillis() - pressedTime < PRESS_AGAIN_TIME) {
                    finish();
                } else {
                    isBackPressed = false;
                    onBackPressed();
                }
            }
        }
    }

    private void checkDeepLink() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }

                        Log.i("ASDadD", "onSuccess: " + deepLink);
                        // Handle the deep link. For example, open the linked
                        // content, or apply promotional credit to the user's
                        // account.
                        // ...

                        // [START_EXCLUDE]
                        // Display deep link in the UI
                        if (deepLink != null) {
                            Log.i("Deep link", "onSuccess: " + deepLink);
                            String [] aLink = deepLink.toString().split(" ");
                            Intent intent = new Intent(MainActivity.this, HyperLinkActivity.class);
                            intent.putExtra(KEY_TRIP_ID, aLink[aLink.length -1]);
                            startActivity(intent);
                        } else {
                            Log.i("Deep link", "getDynamicLink: no link found");
                            navigateToFragment(MapsFragment.newInstance(), false);

                        }
                        // [END_EXCLUDE]
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("asdaf", "getDynamicLink:onFailure", e);
                    }
                });

        // [END get_deep_link]

    }

    private void showLogOutAlert() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(R.string.log_out_dialog);
        alertDialog.setTitle(R.string.log_out_dialog_title);
        alertDialog.setPositiveButton(R.string.positive_button_log_out_dialog, dialogSaveClickListener);
        alertDialog.setNegativeButton(getString(R.string.negative_button_log_out_dialog), dialogCancelClickListener);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.map_title));
        setSupportActionBar(mToolbar);
    }

    private void navigateToFragment(Fragment fragment, boolean back) {
        Utils.navigateToFragment(getSupportFragmentManager(),
                R.id.fragment_container,
                fragment,
                FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                back);
    }

    private void initUI() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        header = navigationView.getHeaderView(NAVIGATION_HEADER_ITEM);
        usernameHeader = (TextView) header.findViewById(R.id.username_header);
    }

    @Override
    public void showToolbar() {
        mToolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideToolBar() {
        mToolbar.setVisibility(View.GONE);
    }

    @Override
    public void setToolbar(Toolbar mToolbar) {
        setSupportActionBar(mToolbar);
    }

    @Override
    public void setTitleToolbar(String titleToolbar) {
        mToolbar.setTitle(titleToolbar);
    }

    @Override
    public void setArrowToolbar() {
        mToolbar.setNavigationIcon(ContextCompat.getDrawable(MainActivity.this, R.drawable.arrow_back_white));
        mToolbar.setNavigationOnClickListener(onClickListener);
    }

    @Override
    public void setDrawerMenu() {
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(selectedListener);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                MainActivity.this,
                mDrawer,
                mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void disabledSwipeDrawer() {
        mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void enabledSwipeDrawer() {
        mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void setLoginUserUI() {
        navigationView.getMenu().getItem(NAVIGATION_LOGIN_ITEM).setVisible(false);
        navigationView.getMenu().getItem(NAVIGATION_LOGOUT_ITEM).setVisible(true);
        user = LocalDBManager.getUser();
        usernameHeader.setText(user.getName());
    }

    @Override
    public void setLogoutUserUI() {
        navigationView.getMenu().getItem(NAVIGATION_LOGIN_ITEM).setVisible(true);
        navigationView.getMenu().getItem(NAVIGATION_LOGOUT_ITEM).setVisible(false);
        user = null;
        usernameHeader.setText(R.string.welcome);
    }

    @Override
    public void setSelectedNavigationIndex(int index) {
        navigationView.getMenu().getItem(index).setChecked(true);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("asdfasdf", "onConnectionFailed: ");
    }
}