package com.camness.vidimguploadjun2016.main;

import android.support.v7.widget.Toolbar;

public interface OnItemChangeCallback {
    void showToolbar();

    void hideToolBar();

    void setToolbar(Toolbar mToolbar);

    void setTitleToolbar(String titleToolbar);

    void setArrowToolbar();

    void setDrawerMenu();

    void disabledSwipeDrawer();

    void enabledSwipeDrawer();

    void setLoginUserUI();

    void setLogoutUserUI();

    void setSelectedNavigationIndex(int index);
}
