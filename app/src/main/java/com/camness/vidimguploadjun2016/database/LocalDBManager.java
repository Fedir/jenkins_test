package com.camness.vidimguploadjun2016.database;

import com.camness.vidimguploadjun2016.model.OverviewPolyline;
import com.camness.vidimguploadjun2016.model.Route;
import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.camness.vidimguploadjun2016.network.response.RouteCamnessResponse;
import com.camness.vidimguploadjun2016.network.response.Trips;
import com.camness.vidimguploadjun2016.network.response.TripsResponse;
import com.camness.vidimguploadjun2016.network.response.UploadedFile;
import com.camness.vidimguploadjun2016.network.response.User;
import com.camness.vidimguploadjun2016.realmModel.DataForUploadTripRealm;
import com.camness.vidimguploadjun2016.realmModel.RouteCamnessRealm;
import com.camness.vidimguploadjun2016.realmModel.RouteCamnessResponseRealm;
import com.camness.vidimguploadjun2016.realmModel.RouteRealm;
import com.camness.vidimguploadjun2016.realmModel.TripRealm;
import com.camness.vidimguploadjun2016.realmModel.TripsResponseRealm;
import com.camness.vidimguploadjun2016.realmModel.UploadInfo;
import com.camness.vidimguploadjun2016.realmModel.UserRealm;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmResults;

public class LocalDBManager {
    private static final Realm realm = Realm.getDefaultInstance();

    public static void saveRoute(Route route) { // save route into DB
        final RouteRealm routeRealm = new RouteRealm(route);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(routeRealm);
            }
        });
    }

    public static ArrayList<Route> loadRoute() { // load route from DB
        RealmResults<RouteRealm> routesRealm = realm.where(RouteRealm.class).findAll();
        ArrayList<Route> routes = new ArrayList<>();
        for (RouteRealm routeRealm : routesRealm) {
            routes.add(new Route(routeRealm));
        }
        return routes;
    }

    public static int getCount() { // get count item DB
        RealmResults<RouteRealm> result = realm.where(RouteRealm.class).findAll();
        return result.size();
    }

    private static boolean isExistDBRoute(RouteRealm routeRealm) { // check if route is exist id DB
        RouteRealm routeExist = realm.where(RouteRealm.class).equalTo("time", routeRealm.getTime()).findFirst();
        return routeExist != null;
    }

    public static void savePolyline(Long time, final OverviewPolyline polyLine) {
        final RouteRealm routesRealm = realm.where(RouteRealm.class).equalTo("time", time).findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                routesRealm.getPolyLine().setPoints(polyLine.getPoints());
            }
        });
    }

    public static void saveUser(User user) {
        final UserRealm userRealm = new UserRealm(user);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(userRealm);
            }
        });
    }

    public static void deleteUserFromDB() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(UserRealm.class).findAll().deleteAllFromRealm();
            }
        });
    }

    public static boolean userIsLoggedIn() {
        UserRealm userRealm = realm.where(UserRealm.class).findFirst();
        return userRealm != null;

    }

    public static User getUser() {
        return new User(realm.where(UserRealm.class).findFirst());
    }

    public static void saveUploadInfo(RouteCamness route, String uri, String userId, String points, String filetype) { // save route into DB
        final RouteCamnessRealm routeCamnessRealm = new RouteCamnessRealm(route);
        final UploadInfo uploadInfo = new UploadInfo(routeCamnessRealm, uri, userId, points, filetype);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(uploadInfo);
            }
        });
    }

    public static UploadInfo loadUploadInfo() { // load route from DB
        UploadInfo uploadInfo = realm.where(UploadInfo.class).findFirst();
        return uploadInfo;
    }

    public static void saveRoutCamness(RouteCamness routeCamness) {
        final RouteCamnessRealm routeCamnessRealm = new RouteCamnessRealm(routeCamness);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(routeCamnessRealm);
            }
        });
    }

    public static ArrayList<RouteCamness> loadRoutCamness() {
        RealmResults<RouteCamnessRealm> routeCamnessRealms = realm.where(RouteCamnessRealm.class).findAll();
        ArrayList<RouteCamness> routeCamnesses = new ArrayList<>();
        for (RouteCamnessRealm routeCamnessRealm : routeCamnessRealms) {
            routeCamnesses.add(new RouteCamness(routeCamnessRealm));
        }
        return routeCamnesses;
    }

    public static void deleteRoutCamness() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(RouteCamnessRealm.class).findAll().deleteAllFromRealm();
            }
        });
    }

    public static void saveTripsResponse(TripsResponse tripsResponse) {
        final TripsResponseRealm tripsResponseRealm = new TripsResponseRealm(tripsResponse);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(tripsResponseRealm);
            }
        });
    }

    public static ArrayList<Trips> getTrips() {
        RealmResults<TripRealm> tripsRealm = realm.where(TripRealm.class).findAll();
        ArrayList<Trips> trips = new ArrayList<>();
        for (TripRealm tripRealm : tripsRealm) {
            trips.add(new Trips(tripRealm));
        }
        return trips;
    }

    public static void deleteTripsResponse() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(TripsResponseRealm.class).findAll().deleteAllFromRealm();
            }
        });
    }

    public static void deleteTrips(){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(TripRealm.class).findAll().deleteAllFromRealm();
            }
        });
    }

    public static void saveRoutCamnessResponse(RouteCamnessResponse routeCamnessResponse) {
        final RouteCamnessResponseRealm routeCamnessResponseRealm = new RouteCamnessResponseRealm(routeCamnessResponse);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(routeCamnessResponseRealm);
            }
        });
    }

    public static void deleteRoutCamnessResponse() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(RouteCamnessResponseRealm.class).findAll().deleteAllFromRealm();
            }
        });
    }

}