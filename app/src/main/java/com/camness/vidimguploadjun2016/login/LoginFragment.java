package com.camness.vidimguploadjun2016.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.database.LocalDBManager;
import com.camness.vidimguploadjun2016.main.OnItemChangeCallback;
import com.camness.vidimguploadjun2016.mapHome.MapsFragment;
import com.camness.vidimguploadjun2016.network.CamnessApiService;
import com.camness.vidimguploadjun2016.network.CancelableCallback;
import com.camness.vidimguploadjun2016.network.response.User;
import com.camness.vidimguploadjun2016.registration.RegisterFragment;
import com.camness.vidimguploadjun2016.resetPassword.ResetPasswordFragment;
import com.camness.vidimguploadjun2016.utils.Utils;

import retrofit2.Call;
import retrofit2.Response;

public class LoginFragment extends Fragment {
    private TextView backTv, registrationTv, resetPasswordTv;
    private TextInputLayout emailEt, passwordEt;
    private Button loginBtn;
    private ProgressDialog loginProgress;
    private OnItemChangeCallback mFragmentsListener;
    private View view;
    private Context mContext;

    private CancelableCallback cancelableCallback = new CancelableCallback() {
        @Override
        public void onResponse(Call call, Response response) {
            navigateToMapAndSaveUser(response);
        }

        @Override
        public void onFailure(Call call, Throwable t) {
            loginProgress.dismiss();
            Toast.makeText(mContext, getString(R.string.try_later), Toast.LENGTH_LONG).show();
        }
    };

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();

            switch (viewId) {
                case R.id.login_btn: {
                    loginUser();
                }
                break;

                case R.id.back_home_tv: {
                    goToFragment(MapsFragment.newInstance());
                }
                break;

                case R.id.show_registration_tv: {
                   goToFragment(RegisterFragment.newInstance());
                }
                break;

                case R.id.show_reset_password_tv: {
                   goToFragment(ResetPasswordFragment.newInstance());
                }
                break;
            }
        }
    };

    private void loginUser(){
        login(emailEt.getEditText().getText().toString(),
                passwordEt.getEditText().getText().toString());
        Utils.hideKeyboard(getContext(), getActivity().getCurrentFocus());
    }

    private void goToFragment(Fragment fragment){
        Utils.navigateToFragment(getActivity().getSupportFragmentManager(),
                R.id.fragment_container,
                fragment,
                FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                true);
        Utils.hideKeyboard(mContext,getActivity().getCurrentFocus());
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        initUI();
        setToolbar();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof OnItemChangeCallback) {
            mFragmentsListener = (OnItemChangeCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mFragmentsListener = null;
    }

    private void login(String email, String password) {
        loginProgress.show();
        if (!validateCredentials(email, password)) {
            CamnessApiService.login(email, password, cancelableCallback);
        } else {
            loginProgress.dismiss();
        }
    }

    private void navigateToMapAndSaveUser(Response response) {
        User user = (User) response.body();
        loginProgress.dismiss();
        if (user.getAuth().equals(getString(R.string.true_statement))) {
            Toast.makeText(mContext, R.string.auth_successfully, Toast.LENGTH_LONG).show();
            LocalDBManager.saveUser(user);
            Utils.navigateToFragment(getActivity().getSupportFragmentManager(),
                    R.id.fragment_container,
                    MapsFragment.newInstance(),
                    FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                    false);
        } else {
            Toast.makeText(mContext, R.string.auth_failed, Toast.LENGTH_LONG).show();
        }
    }

    private boolean validateCredentials(String email, String password) {
        boolean error = false;
        emailEt.setErrorEnabled(false);
        passwordEt.setErrorEnabled(false);

        if (TextUtils.isEmpty(email)) {
            emailEt.setError(getString(R.string.empty_email));
            error = true;
        } else if (TextUtils.isEmpty(password)) {
            passwordEt.setError(getString(R.string.empty_password));
            error = true;
        }
        return error;
    }

    private void setToolbar() {
        mFragmentsListener.hideToolBar();
        mFragmentsListener.disabledSwipeDrawer();
    }

    private void initUI() {
        backTv = (TextView) view.findViewById(R.id.back_home_tv);
        loginBtn = (Button) view.findViewById(R.id.login_btn);
        emailEt = (TextInputLayout) view.findViewById(R.id.email_til);
        passwordEt = (TextInputLayout) view.findViewById(R.id.password_til);
        registrationTv = (TextView) view.findViewById(R.id.show_registration_tv);
        resetPasswordTv = (TextView) view.findViewById(R.id.show_reset_password_tv);
        loginProgress = new ProgressDialog(mContext);
        loginProgress.setCanceledOnTouchOutside(false);
        loginProgress.setCancelable(false);
        loginProgress.setMessage(getString(R.string.waite_please));
        loginBtn.setOnClickListener(onClickListener);
        registrationTv.setOnClickListener(onClickListener);
        backTv.setOnClickListener(onClickListener);
        resetPasswordTv.setOnClickListener(onClickListener);
    }
}