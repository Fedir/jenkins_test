package com.camness.vidimguploadjun2016.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class TripsResponse {

    @SerializedName("message")
    @Expose
    public Boolean message;
    @SerializedName("data")
    @Expose
    public List<Trips> data = new ArrayList<>();
    @SerializedName("status_code")
    @Expose
    public Integer statusCode;

    public TripsResponse(Boolean message, List<Trips> data, Integer statusCode) {
        this.message = message;
        this.data = data;
        this.statusCode = statusCode;
    }

    public Boolean getMessage() {
        return message;
    }

    public void setMessage(Boolean message) {
        this.message = message;
    }

    public List<Trips> getData() {
        return data;
    }

    public void setData(List<Trips> data) {
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
