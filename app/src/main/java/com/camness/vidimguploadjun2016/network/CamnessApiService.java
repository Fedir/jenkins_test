package com.camness.vidimguploadjun2016.network;

import com.camness.vidimguploadjun2016.network.model.ShareTripBody;
import com.camness.vidimguploadjun2016.network.model.TripData;
import com.camness.vidimguploadjun2016.network.model.UploadTripBody;
import com.camness.vidimguploadjun2016.network.response.UploadResponse;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;

public class CamnessApiService {
    private static String VIDEO_URL = "/video/";
    private static final String MULTIPART_DATA = "multipart/form-data";

    public static void getVideo(String category, CancelableCallback callback) {
        RetrofitBuilder.getCamnessApi().getVideo(VIDEO_URL + category).enqueue(callback);
    }

    public static void getVideoByCategory(int distance, String latLng, String category, String startDate, String endDate, CancelableCallback callback) {
        String url = VIDEO_URL + distance + "/" + latLng + "/" + category + "/" + startDate + "/" + endDate;
        RetrofitBuilder.getCamnessApi().getVideoByCategory(url).enqueue(callback);
    }

    public static void login(String email, String password, CancelableCallback callback) {
        RequestBody rEmail = RequestBody.create(MediaType.parse(MULTIPART_DATA), email);
        RequestBody rPassword = RequestBody.create(MediaType.parse(MULTIPART_DATA), password);

        RetrofitBuilder.getCamnessApi().logIn(rEmail, rPassword).enqueue(callback);
    }

    public static void singUp(String name, String email, String phoneNumber, String password, CancelableCallback callback) {
        RequestBody rName = RequestBody.create(MediaType.parse(MULTIPART_DATA), name);
        RequestBody rEmail = RequestBody.create(MediaType.parse(MULTIPART_DATA), email);
        RequestBody rNumber = RequestBody.create(MediaType.parse(MULTIPART_DATA), phoneNumber);
        RequestBody rPassword = RequestBody.create(MediaType.parse(MULTIPART_DATA), password);

        RetrofitBuilder.getCamnessApi().singUp(rName, rEmail, rNumber, rPassword).enqueue(callback);
    }

    public static void uploadFile(String description, String date, String time, String category, String xCord, String yCord, String userId, String multiCoord, File image, Callback<UploadResponse> callback) {
        RequestBody rImage = RequestBody.create(MediaType.parse(MULTIPART_DATA), image);
        MultipartBody.Part mrImage = MultipartBody.Part.createFormData("image", image.getName(), rImage);
        RequestBody rDescription = RequestBody.create(MediaType.parse(MULTIPART_DATA), description);
        RequestBody rDate = RequestBody.create(MediaType.parse(MULTIPART_DATA), date);
        RequestBody rTime = RequestBody.create(MediaType.parse(MULTIPART_DATA), time);
        RequestBody rCategory = RequestBody.create(MediaType.parse(MULTIPART_DATA), category);
        RequestBody rxCord = RequestBody.create(MediaType.parse(MULTIPART_DATA), xCord);
        RequestBody ryCord = RequestBody.create(MediaType.parse(MULTIPART_DATA), yCord);
        RequestBody rUserId = RequestBody.create(MediaType.parse(MULTIPART_DATA), userId);
        RequestBody rMultiCoord = RequestBody.create(MediaType.parse(MULTIPART_DATA), multiCoord);
        RetrofitBuilder.getCamnessApi().uploadRouteFile(rDescription, rDate, rTime, rCategory, rxCord, ryCord, rUserId, rMultiCoord, mrImage).enqueue(callback);
    }

    public static void uploadFileWithoutFile(String description, String date, String time, String category, String xCord, String yCord, String userId, String multiCoord, Callback<UploadResponse> callback) {
        RequestBody rDescription = RequestBody.create(MediaType.parse(MULTIPART_DATA), description);
        RequestBody rDate = RequestBody.create(MediaType.parse(MULTIPART_DATA), date);
        RequestBody rTime = RequestBody.create(MediaType.parse(MULTIPART_DATA), time);
        RequestBody rCategory = RequestBody.create(MediaType.parse(MULTIPART_DATA), category);
        RequestBody rxCord = RequestBody.create(MediaType.parse(MULTIPART_DATA), xCord);
        RequestBody ryCord = RequestBody.create(MediaType.parse(MULTIPART_DATA), yCord);
        RequestBody rUserId = RequestBody.create(MediaType.parse(MULTIPART_DATA), userId);
        RequestBody rMultiCoord = RequestBody.create(MediaType.parse(MULTIPART_DATA), multiCoord);
        RetrofitBuilder.getCamnessApi().uploadFileWithoutFile(rDescription, rDate, rTime, rCategory, rxCord, ryCord, rUserId, rMultiCoord).enqueue(callback);
    }

    public static void resetPassword(String email, CancelableCallback callback) {
        RequestBody rEmail = RequestBody.create(MediaType.parse(MULTIPART_DATA), email);

        RetrofitBuilder.getCamnessApi().resetPassword(rEmail).enqueue(callback);
    }

    public static void uploadTrip(TripData tripData, String name, String description, Callback<UploadResponse> callback) {
        UploadTripBody uploadTripBody = new UploadTripBody(tripData, name, description);
        RetrofitBuilder.getCamnessApi().uploadTrip(uploadTripBody).enqueue(callback);
    }

    public static void shareTrip(String tripId, String userEmail, CancelableCallback callback) {
        ShareTripBody shareTripBody = new ShareTripBody(tripId, userEmail);
        RetrofitBuilder.getCamnessApi().shareTrip(shareTripBody).enqueue(callback);
    }

    public static void getTrips(CancelableCallback callback) {
        RetrofitBuilder.getCamnessApi().getTrips().enqueue(callback);
    }

    public static void getUploadedFiles(CancelableCallback callback) {
        RetrofitBuilder.getCamnessApi().getUploadedFiles().enqueue(callback);
    }

    public static void deleteTrip(String tripId, CancelableCallback callback) {
        RetrofitBuilder.getCamnessApi().deleteTrip(tripId).enqueue(callback);
    }
}
