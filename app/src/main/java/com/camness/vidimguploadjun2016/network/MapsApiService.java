package com.camness.vidimguploadjun2016.network;

import com.camness.vidimguploadjun2016.model.Point;
import com.camness.vidimguploadjun2016.network.response.RouteResponse;

import java.util.ArrayList;

import retrofit2.Callback;

public class MapsApiService {
    public static final String API_KEY = "AIzaSyDoWa-Eauf0Pe5ZhHyTKr8iSj_29erc7bo";

    public static void getRoute(String position, String destination, ArrayList<Point> wayPoints, Callback<RouteResponse> callback) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < wayPoints.size(); i++) {
            sb.append(wayPoints.get(i).getmLatitude() + ",");
            sb.append(wayPoints.get(i).getmLongitude() + "|");
        }
        RetrofitBuilder.getMapsApi().getRoute(position, destination, sb.toString(), "walking", API_KEY).enqueue(callback);
    }
}
