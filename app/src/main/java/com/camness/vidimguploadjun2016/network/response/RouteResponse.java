package com.camness.vidimguploadjun2016.network.response;

import com.camness.vidimguploadjun2016.model.Route;

import java.util.ArrayList;

public class RouteResponse {
    public ArrayList<Route> routes;

    public Route getPoints() {
        return this.routes.get(0);
    }
}
