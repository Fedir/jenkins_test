package com.camness.vidimguploadjun2016.network.model;


public class UploadTripBody {
    private TripData tripData;
    private String name;
    private String description;

    public UploadTripBody(TripData tripData, String name, String description) {
        this.tripData = tripData;
        this.name = name;
        this.description = description;

    }

    public TripData getTripData() {
        return tripData;
    }

    public void setTripData(TripData tripData) {
        this.tripData = tripData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}