package com.camness.vidimguploadjun2016.network.model;


public class ShareTripBody {

  private String id;
  private String userEmail;

  public ShareTripBody (String id, String userEmail) {
      this.id = id;
      this.userEmail = userEmail;
  }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
