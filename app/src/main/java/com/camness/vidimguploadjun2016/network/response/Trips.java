package com.camness.vidimguploadjun2016.network.response;


import com.camness.vidimguploadjun2016.realmModel.TripRealm;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Trips {

    @SerializedName("tripId")
    @Expose
    private String tripId;
    @SerializedName("tripName")
    @Expose
    private String tripName;
    @SerializedName("tripDescription")
    @Expose
    private String tripDescription;
    @SerializedName("isOwn")
    @Expose
    private boolean isOwn;
    @SerializedName("multiCoords")
    @Expose
    private List<String> multiCoords;

    private String link;

    public Trips(String tripId, String tripName, String tripDescription, boolean isOwn, String link, ArrayList<String> multiCoords) {
        this.tripId = tripId;
        this.tripName = tripName;
        this.tripDescription = tripDescription;
        this.isOwn = isOwn;
        this.link = link;
        this.multiCoords = multiCoords;
    }

    public Trips (TripRealm tripRealm){
        setTripId(tripRealm.getTripId());
        setTripName(tripRealm.getTripName());
        setTripDescription(tripRealm.getTripDescription());
        setOwn(tripRealm.isOwn());
        setLink(tripRealm.getLink());
        setMultiCoords(tripRealm.getMultiCoord());
    }

    public String getTripId() {
        return tripId;
    }

    private void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getLink() {
        return link;
    }

    private void setLink(String link) {
        this.link = link;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getTripDescription() {
        return tripDescription;
    }

    public void setTripDescription(String tripDescription) {
        this.tripDescription = tripDescription;
    }

    public boolean isOwn() {
        return isOwn;
    }

    private void setOwn(boolean own) {
        isOwn = own;
    }

    public List<String> getMultiCoords() {
        return multiCoords;
    }

    public void setMultiCoords(List<String> multiCoords) {
        this.multiCoords = multiCoords;
    }
}
