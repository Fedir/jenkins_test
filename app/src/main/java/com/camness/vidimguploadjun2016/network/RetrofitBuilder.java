package com.camness.vidimguploadjun2016.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {
    private static Retrofit retrofitMaps;
    private static Retrofit retrofitCamness;
    private static MapsApi mapsApi = getRetrofitMapsBuilder().create(MapsApi.class);
    private static CamnessApi camnessApi = getRetrofitCamnessBuilder().create(CamnessApi.class);

    private RetrofitBuilder() {
    }

    private static Retrofit getRetrofitMapsBuilder() {
        if (retrofitMaps == null) {
            retrofitMaps = new Retrofit.Builder()
                    .baseUrl(MapsApi.BASE_URL)
                    .client(getClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitMaps;
    }

    private static Retrofit getRetrofitCamnessBuilder() {
        if (retrofitCamness == null) {
            retrofitCamness = new Retrofit.Builder()
                    .baseUrl(CamnessApi.BASE_URL)
                    .client(getClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitCamness;
    }

    public static MapsApi getMapsApi() {
        return mapsApi;
    }

    public static CamnessApi getCamnessApi() {
        return camnessApi;
    }

    private static OkHttpClient getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().connectTimeout(500,TimeUnit.SECONDS).readTimeout(500,TimeUnit.SECONDS).writeTimeout(500,TimeUnit.SECONDS).addInterceptor(interceptor).build();
    }
}
