package com.camness.vidimguploadjun2016.network;

import com.camness.vidimguploadjun2016.network.response.RouteResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MapsApi {
    String BASE_URL = "https://maps.googleapis.com/maps/api/directions/";

    @GET("/maps/api/directions/json")
    Call<RouteResponse> getRoute(
            @Query("origin") String position,
            @Query("destination") String destination,
            @Query("waypoints") String waypoints,
            @Query("mode") String mode,
            @Query("key") String key);
}
