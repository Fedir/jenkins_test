package com.camness.vidimguploadjun2016.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UploadedFile {

    @SerializedName("fileId")
    @Expose
    private String fileId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("uploadedBy")
    @Expose
    private String uploadedBy;

    @SerializedName("multiCoOrdinates")
    @Expose
    private String multiCoOrdinates;

    private boolean isChecked;

    public UploadedFile(String fileId, String description, String date, String time,
                        String type, String uploadedBy, String multiCoOrdinates) {
        this.fileId = fileId;
        this.description = description;
        this.date = date;
        this.time = time;
        this.type = type;
        this.uploadedBy = uploadedBy;
        this.multiCoOrdinates = multiCoOrdinates;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getMultiCoOrdinates() {
        return multiCoOrdinates;
    }

    public void setMultiCoOrdinates(String multiCoOrdinates) {
        this.multiCoOrdinates = multiCoOrdinates;
    }
}
