package com.camness.vidimguploadjun2016.network;

import com.camness.vidimguploadjun2016.network.model.ShareTripBody;
import com.camness.vidimguploadjun2016.network.model.UploadTripBody;
import com.camness.vidimguploadjun2016.network.response.DeleteResponse;
import com.camness.vidimguploadjun2016.network.response.RegistrationResponse;
import com.camness.vidimguploadjun2016.network.response.RouteCamnessResponse;
import com.camness.vidimguploadjun2016.network.response.Trips;
import com.camness.vidimguploadjun2016.network.response.UploadResponse;
import com.camness.vidimguploadjun2016.network.response.UploadedFilesResponse;
import com.camness.vidimguploadjun2016.network.response.User;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface CamnessApi {
    String BASE_URL = "https://camness.in/";

    @GET
    Call<RouteCamnessResponse> getVideo(@Url String url);

    @GET
    Call<RouteCamnessResponse> getVideoByCategory(@Url String url);

    @Multipart
    @POST("/androidLogin")
    Call<User> logIn(
            @Part("email") RequestBody body,
            @Part("password") RequestBody password);

    @Multipart
    @POST("/androidRegister")
    Call<RegistrationResponse> singUp(
            @Part("name") RequestBody name,
            @Part("email") RequestBody email,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("password") RequestBody password);

    @Multipart
    @POST("/android")
    Call<UploadResponse> uploadFileWithoutFile(
            @Part("description") RequestBody description,
            @Part("date") RequestBody date,
            @Part("time") RequestBody time,
            @Part("category") RequestBody category,
            @Part("xCord") RequestBody xCord,
            @Part("yCord") RequestBody yCord,
            @Part("userId") RequestBody userId,
            @Part("multiCoOrdinates") RequestBody multiCoordinates);

    @Streaming
    @Multipart
    @POST("/android")
    Call<UploadResponse> uploadRouteFile(
            @Part("description") RequestBody description,
            @Part("date") RequestBody date,
            @Part("time") RequestBody time,
            @Part("category") RequestBody category,
            @Part("xCord") RequestBody xCord,
            @Part("yCord") RequestBody yCord,
            @Part("userId") RequestBody userId,
            @Part("multiCoOrdinates") RequestBody multiCoordinates,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("/password/email")
    Call<Object> resetPassword(@Part("email") RequestBody email);


    @Headers("token")
    @GET("getTrips")
    Call<Trips> getTrips();

    @Headers("token")
    @GET("getUploadedFiles")
    Call<UploadedFilesResponse> getUploadedFiles();

    @Headers("token")
    @POST("uploadTrip")
    Call<UploadResponse> uploadTrip(@Body UploadTripBody body);

    @Headers("token")
    @POST("shareTrip")
    Call<Object> shareTrip(@Body ShareTripBody body);

    @Headers("token")
    @DELETE ("deleteTrip/{id}")
    Call<DeleteResponse> deleteTrip (@Path("id") String id);
}