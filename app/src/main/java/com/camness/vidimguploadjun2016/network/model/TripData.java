package com.camness.vidimguploadjun2016.network.model;


import java.util.ArrayList;

public class TripData {

    private ArrayList<String> fileIds = new ArrayList<>();
    private ArrayList<String> fileMultiCoords = new ArrayList<>();


    public ArrayList<String> getFileIds() {
        return fileIds;
    }

    public void setFileIds(ArrayList<String> fileIds) {
        this.fileIds = fileIds;
    }

    public ArrayList<String> getFileMultiCoords() {
        return fileMultiCoords;
    }

    public void setFileMultiCoords(ArrayList<String> fileMultiCoords) {
        this.fileMultiCoords = fileMultiCoords;
    }


    public void addFileId(String fileId) {
        fileIds.add(fileId);
    }

    public void removeFileId(String fileId) {
        fileIds.remove(fileId);
    }

    public void addFileMultiCoords(String multiCoords) {
        fileMultiCoords.add(multiCoords);
    }

    public void removeMultiCoords(String multiCoords) {
        fileMultiCoords.remove(multiCoords);
    }
}
