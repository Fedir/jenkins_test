package com.camness.vidimguploadjun2016.network.response;

import com.camness.vidimguploadjun2016.realmModel.UserRealm;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("auth")
    @Expose
    public String auth;

    public User() {
    }

    public User(UserRealm userRealm) {
        setName(userRealm.getName());
        setId(userRealm.getId());
        setAuth(userRealm.getAuth());
        setLat(userRealm.getLat());
        setLng(userRealm.getLng());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
}
