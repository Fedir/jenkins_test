package com.camness.vidimguploadjun2016.network.response;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.camness.vidimguploadjun2016.model.Point;
import com.camness.vidimguploadjun2016.realmModel.PointRealm;
import com.camness.vidimguploadjun2016.realmModel.RouteCamnessRealm;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RouteCamness implements Parcelable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("videoDescription")
    @Expose
    public String videoDescription;
    @SerializedName("xCord")
    @Expose
    public String xCord;
    @SerializedName("yCord")
    @Expose
    public String yCord;
    @SerializedName("extension")
    @Expose
    public String extension;
    @SerializedName("folderName")
    @Expose
    public String folderName;
    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("uploaderName")
    @Expose
    public String uploaderName;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("views")
    @Expose
    public String views;
    @SerializedName("likes")
    @Expose
    public String likes;
    @SerializedName("disLikes")
    @Expose
    public String disLikes;
    @SerializedName("publicOrPrivate")
    @Expose
    public String publicOrPrivate;
    @SerializedName("markerImage")
    @Expose
    public String markerImage;
    @SerializedName("fileType")
    @Expose
    public String fileType;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("currentPlaylist")
    @Expose
    public String currentPlaylist;
    @SerializedName("multiCoOrdinates")
    @Expose
    public String multiCoOrdinates;

    private ArrayList<Point> points = new ArrayList<>();

    public RouteCamness() {
    }

    public RouteCamness(RouteCamnessRealm routeCamnessRealm) {
        setId(routeCamnessRealm.getId());
        setName(routeCamnessRealm.getName());
        setTitle(routeCamnessRealm.getTitle());
        setVideoDescription(routeCamnessRealm.getVideoDescription());
        setxCord(routeCamnessRealm.getxCord());
        setyCord(routeCamnessRealm.getyCord());
        setExtension(routeCamnessRealm.getExtension());
        setFolderName(routeCamnessRealm.getFolderName());
        setUserId(routeCamnessRealm.getUserId());
        setUploaderName(routeCamnessRealm.getUploaderName());
        setCategory(routeCamnessRealm.getCategory());
        setViews(routeCamnessRealm.getViews());
        setLikes(routeCamnessRealm.getLikes());
        setDisLikes(routeCamnessRealm.getDisLikes());
        setCurrentPlaylist(routeCamnessRealm.getCurrentPlaylist());
        setFileType(routeCamnessRealm.getFileType());
        setDate(routeCamnessRealm.getDate());
        setTime(routeCamnessRealm.getTime());
        setPublicOrPrivate(routeCamnessRealm.getPublicOrPrivate());
        setMarkerImage(routeCamnessRealm.getMarkerImage());
        setMultiCoOrdinates(routeCamnessRealm.getMultiCoOrdinates());

        for (PointRealm pointRealm : routeCamnessRealm.getPoints()) {
            points.add(new Point(pointRealm));
        }
    }

    protected RouteCamness(Parcel in) {
        id = in.readString();
        name = in.readString();
        title = in.readString();
        videoDescription = in.readString();
        xCord = in.readString();
        yCord = in.readString();
        extension = in.readString();
        folderName = in.readString();
        userId = in.readString();
        uploaderName = in.readString();
        category = in.readString();
        views = in.readString();
        likes = in.readString();
        disLikes = in.readString();
        publicOrPrivate = in.readString();
        markerImage = in.readString();
        fileType = in.readString();
        date = in.readString();
        time = in.readString();
        currentPlaylist = in.readString();
        multiCoOrdinates = in.readString();
        points = in.createTypedArrayList(Point.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(title);
        dest.writeString(videoDescription);
        dest.writeString(xCord);
        dest.writeString(yCord);
        dest.writeString(extension);
        dest.writeString(folderName);
        dest.writeString(userId);
        dest.writeString(uploaderName);
        dest.writeString(category);
        dest.writeString(views);
        dest.writeString(likes);
        dest.writeString(disLikes);
        dest.writeString(publicOrPrivate);
        dest.writeString(markerImage);
        dest.writeString(fileType);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(currentPlaylist);
        dest.writeString(multiCoOrdinates);
        dest.writeTypedList(points);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RouteCamness> CREATOR = new Creator<RouteCamness>() {
        @Override
        public RouteCamness createFromParcel(Parcel in) {
            return new RouteCamness(in);
        }

        @Override
        public RouteCamness[] newArray(int size) {
            return new RouteCamness[size];
        }
    };

    public String getMultiCoOrdinates() {
        return multiCoOrdinates;
    }

    public void setMultiCoOrdinates(String multiCoOrdinates) {
        this.multiCoOrdinates = multiCoOrdinates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getxCord() {
        return xCord;
    }

    public void setxCord(String xCord) {
        this.xCord = xCord;
    }

    public String getyCord() {
        return yCord;
    }

    public void setyCord(String yCord) {
        this.yCord = yCord;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUploaderName() {
        return uploaderName;
    }

    public void setUploaderName(String uploaderName) {
        this.uploaderName = uploaderName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDisLikes() {
        return disLikes;
    }

    public void setDisLikes(String disLikes) {
        this.disLikes = disLikes;
    }

    public String getPublicOrPrivate() {
        return publicOrPrivate;
    }

    public void setPublicOrPrivate(String publicOrPrivate) {
        this.publicOrPrivate = publicOrPrivate;
    }

    public String getMarkerImage() {
        return markerImage;
    }

    public void setMarkerImage(String markerImage) {
        this.markerImage = markerImage;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCurrentPlaylist() {
        return currentPlaylist;
    }

    public void setCurrentPlaylist(String currentPlaylist) {
        this.currentPlaylist = currentPlaylist;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<Point> points) {
        Log.d("TAG", "setPoints: " + points.size());
        this.points = points;
    }

    public long getDateTime() {
        return Long.parseLong(date.replace("-","") + time.replace(":" , ""));
    }
}
