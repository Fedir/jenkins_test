package com.camness.vidimguploadjun2016.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.camness.vidimguploadjun2016.realmModel.OverviewPolylineRealm;

public class OverviewPolyline implements Parcelable {
    private String points;

    public OverviewPolyline() {
    }

    public OverviewPolyline(OverviewPolylineRealm realm) {
        setPoints(realm.getPoints());
    }

    protected OverviewPolyline(Parcel in) {
        points = in.readString();
    }

    public static final Creator<OverviewPolyline> CREATOR = new Creator<OverviewPolyline>() {
        @Override
        public OverviewPolyline createFromParcel(Parcel in) {
            return new OverviewPolyline(in);
        }

        @Override
        public OverviewPolyline[] newArray(int size) {
            return new OverviewPolyline[size];
        }
    };

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(points);
    }
}
