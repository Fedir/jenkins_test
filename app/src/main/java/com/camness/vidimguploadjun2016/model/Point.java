package com.camness.vidimguploadjun2016.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.camness.vidimguploadjun2016.realmModel.PointRealm;

public class Point implements Parcelable {
    private double mLatitude;
    private double mLongitude;

    public Point() {}

    public Point(double mLatitude, double mLongitude) {
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
    }

    public Point(PointRealm pointRealm) {
        setmLatitude(pointRealm.getmLatitude());
        setmLongitude(pointRealm.getmLongitude());
    }

    protected Point(Parcel in) {
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
    }

    public static final Creator<Point> CREATOR = new Creator<Point>() {
        @Override
        public Point createFromParcel(Parcel in) {
            return new Point(in);
        }

        @Override
        public Point[] newArray(int size) {
            return new Point[size];
        }
    };

    public double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
    }
}
