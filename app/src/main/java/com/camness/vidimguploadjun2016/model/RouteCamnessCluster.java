package com.camness.vidimguploadjun2016.model;
import android.os.Parcel;
import android.os.Parcelable;

import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class RouteCamnessCluster implements ClusterItem, Parcelable {
    RouteCamness routeCamness;

    public RouteCamnessCluster(RouteCamness routeCamness) {
        this.routeCamness = routeCamness;
    }

    protected RouteCamnessCluster(Parcel in) {
        routeCamness = in.readParcelable(RouteCamness.class.getClassLoader());
    }

    public static final Creator<RouteCamnessCluster> CREATOR = new Creator<RouteCamnessCluster>() {
        @Override
        public RouteCamnessCluster createFromParcel(Parcel in) {
            return new RouteCamnessCluster(in);
        }

        @Override
        public RouteCamnessCluster[] newArray(int size) {
            return new RouteCamnessCluster[size];
        }
    };

    @Override
    public LatLng getPosition() {
        return new LatLng(Double.parseDouble(routeCamness.getxCord()),Double.parseDouble(routeCamness.getyCord()));
    }

    public RouteCamness getRouteCamness() {
        return routeCamness;
    }

    public void setRouteCamness(RouteCamness routeCamness) {
        this.routeCamness = routeCamness;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(routeCamness, i);
    }

    public long getDateTime() {
        String i = routeCamness.getDate().replace("-","") + routeCamness.getTime().replace(":" , "");
        long v = Long.parseLong(i);
        return v;
    }
}
