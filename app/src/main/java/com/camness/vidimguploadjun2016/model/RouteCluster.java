package com.camness.vidimguploadjun2016.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class RouteCluster implements ClusterItem, Parcelable{
    private Route route;

    public RouteCluster() {}

    public RouteCluster(Route route) {
        this.route = route;
    }

    protected RouteCluster(Parcel in) {
        route = in.readParcelable(Route.class.getClassLoader());
    }

    public static final Creator<RouteCluster> CREATOR = new Creator<RouteCluster>() {
        @Override
        public RouteCluster createFromParcel(Parcel in) {
            return new RouteCluster(in);
        }

        @Override
        public RouteCluster[] newArray(int size) {
            return new RouteCluster[size];
        }
    };

    @Override
    public LatLng getPosition() {
        return new LatLng(route.getPoints().get(0).getmLatitude(),route.getPoints().get(0).getmLongitude());
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(route, flags);
    }
}
