package com.camness.vidimguploadjun2016.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.camness.vidimguploadjun2016.realmModel.PointRealm;
import com.camness.vidimguploadjun2016.realmModel.RouteRealm;
import java.util.ArrayList;

public class Route implements Parcelable {
    private String videoURL;
    private long time;
    private ArrayList<Point> points = new ArrayList<>();
    private OverviewPolyline overview_polyline = new OverviewPolyline();

    protected Route(Parcel in) {
        videoURL = in.readString();
        time = in.readLong();
        points = in.createTypedArrayList(Point.CREATOR);
        overview_polyline = in.readParcelable(OverviewPolyline.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoURL);
        dest.writeLong(time);
        dest.writeTypedList(points);
        dest.writeParcelable(overview_polyline, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Route> CREATOR = new Creator<Route>() {
        @Override
        public Route createFromParcel(Parcel in) {
            return new Route(in);
        }

        @Override
        public Route[] newArray(int size) {
            return new Route[size];
        }
    };

    public OverviewPolyline getPolyLine() {
        return overview_polyline;
    }

    public void setPolyLine(OverviewPolyline polyline) {
        overview_polyline = polyline;
    }

    public Route() {
    }

    public Route(ArrayList<Point> points, long time) {
        this.points = points;
        this.time = time;
    }

    public Route(RouteRealm routeRealm) {
        for (PointRealm pointRealm : routeRealm.getPointRealms()) {
            points.add(new Point(pointRealm));
        }
        setTime(routeRealm.getTime());
        setVideoURL(getVideoURL());
        setPolyLine(new OverviewPolyline(routeRealm.getPolyLine()));
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<Point> points) {
        this.points = points;
    }

    public OverviewPolyline getOverview_polyline() {
        return overview_polyline;
    }

    public void setOverview_polyline(OverviewPolyline overview_polyline) {
        this.overview_polyline = overview_polyline;
    }
}
