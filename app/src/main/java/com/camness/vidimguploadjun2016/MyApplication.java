package com.camness.vidimguploadjun2016;

import android.app.Application;
import com.camness.vidimguploadjun2016.model.Point;
import java.util.ArrayList;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {

    private static ArrayList<Point> routePoints = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        configurationRealm();
    }

    private void configurationRealm() {

        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();

        /*RealmConfiguration realmConfig = new RealmConfiguration // configuration DB realm
                .Builder(getApplicationContext())
                .deleteRealmIfMigrationNeeded()
                .build();*/

        Realm.setDefaultConfiguration(config);

        //clearRealm(realmConfig);
    }

    private void clearRealm(RealmConfiguration config) {
        Realm realm = Realm.getInstance(config);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

    public static ArrayList<Point> getRoutePoints() {
        return routePoints;
    }

    public static void setRoutePoints(ArrayList<Point> routePoints) {
        MyApplication.routePoints = routePoints;
    }
}
