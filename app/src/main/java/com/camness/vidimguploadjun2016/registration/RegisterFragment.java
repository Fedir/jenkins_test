package com.camness.vidimguploadjun2016.registration;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.main.OnItemChangeCallback;
import com.camness.vidimguploadjun2016.mapHome.MapsFragment;
import com.camness.vidimguploadjun2016.network.CamnessApiService;
import com.camness.vidimguploadjun2016.network.CancelableCallback;
import com.camness.vidimguploadjun2016.network.response.RegistrationResponse;
import com.camness.vidimguploadjun2016.utils.Utils;

import retrofit2.Call;
import retrofit2.Response;

public class RegisterFragment extends Fragment {
    private TextView alreadyAccountTv;
    private TextInputLayout nameEt, emailEt, passwordEt, confirmPasswordEt, phoneNumberEt;
    private Button registerBtn;
    private ProgressDialog registerProgress;
    private OnItemChangeCallback mFragmentsListener;
    private View view;
    private Context mContext;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();

            switch (viewId) {
                case R.id.register_btn: {
                    setUserRegisterData();
                }
                break;

                case R.id.already_have_account_tv: {
                    getActivity().onBackPressed();
                    Utils.hideKeyboard(mContext,getActivity().getCurrentFocus());
                }
                break;
            }
        }
    };

    private CancelableCallback cancelableCallback = new CancelableCallback() {
        @Override
        public void onFailure(Call call, Throwable t) {
            registerProgress.dismiss();
            Toast.makeText(mContext, R.string.try_later, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onResponse(Call call, Response response) {
            registerProgress.dismiss();
            if (response.body() != null) {
                navigateToMap(response);

            } else {
                Toast.makeText(mContext, R.string.something_wrong, Toast.LENGTH_LONG).show();
            }
        }
    };

    private void setUserRegisterData(){
        String email = emailEt.getEditText().getText().toString();
        String name = nameEt.getEditText().getText().toString();
        String password = passwordEt.getEditText().getText().toString();
        String confirmPassword = confirmPasswordEt.getEditText().getText().toString();
        String phoneNumber = phoneNumberEt.getEditText().getText().toString();
        setErrorDisabled();
        singUp(email, name, password, confirmPassword, phoneNumber);
    }

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_register, container, false);
        initUI();
        setToolbar();
        setTextChangeListener();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof OnItemChangeCallback) {
            mFragmentsListener = (OnItemChangeCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mFragmentsListener = null;
    }

    private void singUp(String email, String name, String password, String confirmPassword, String phoneNumber) {
        registerProgress.show();
        if (isValidCredentials(email, name, password, confirmPassword, phoneNumber)) {
            CamnessApiService.singUp(name, email, phoneNumber, password,cancelableCallback);
        } else {
            registerProgress.dismiss();
        }
    }

    private void navigateToMap(Response response) {
        RegistrationResponse register = (RegistrationResponse) response.body();
        if (register.auth.equals("true")) {
            Toast.makeText(mContext, R.string.register_successfully, Toast.LENGTH_LONG).show();
            Utils.navigateToFragment(getActivity().getSupportFragmentManager(),
                    R.id.fragment_container,
                    MapsFragment.newInstance(),
                    FragmentTransaction.TRANSIT_FRAGMENT_OPEN,
                    false);
        } else {
            Toast.makeText(mContext, register.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private boolean isValidCredentials(String email, String name, String password, String confirmPassword, String phoneNumber) {
        if (TextUtils.isEmpty(name)) {
            nameEt.setError(getString(R.string.empty_name));
            return false;

        } else if (TextUtils.isEmpty(email)) {
            emailEt.setError(getString(R.string.empty_email));
            return false;

        } else if (!Utils.isValidEmail(email)) {
            emailEt.setError(getString(R.string.email_incorrect));
            return false;

        } else if (TextUtils.isEmpty(phoneNumber)) {
            phoneNumberEt.setError(getString(R.string.empty_number));
            return false;

        } else if (TextUtils.isEmpty(password)) {
            passwordEt.setError(getString(R.string.empty_password));
            return false;

        } else if (password.length() < 6) {
            passwordEt.setError(getString(R.string.longer_password));
            return false;

        } else if (!password.equals(confirmPassword)) {
            confirmPasswordEt.setError(getString(R.string.password_and_confirm_match));
            return false;
        }

        return true;
    }

    private void setTextChangeListener() {
        TextWatcher changeText = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setErrorDisabled();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        emailEt.getEditText().addTextChangedListener(changeText);
        nameEt.getEditText().addTextChangedListener(changeText);
        passwordEt.getEditText().addTextChangedListener(changeText);
        confirmPasswordEt.getEditText().addTextChangedListener(changeText);
        phoneNumberEt.getEditText().addTextChangedListener(changeText);
    }

    private void setErrorDisabled() {
        emailEt.setErrorEnabled(false);
        nameEt.setErrorEnabled(false);
        passwordEt.setErrorEnabled(false);
        confirmPasswordEt.setErrorEnabled(false);
        phoneNumberEt.setErrorEnabled(false);
    }

    private void setToolbar() {
        mFragmentsListener.hideToolBar();
        mFragmentsListener.disabledSwipeDrawer();
    }

    private void initUI() {
        alreadyAccountTv = (TextView) view.findViewById(R.id.already_have_account_tv);
        nameEt = (TextInputLayout) view.findViewById(R.id.username_et);
        emailEt = (TextInputLayout) view.findViewById(R.id.email_register_et);
        passwordEt = (TextInputLayout) view.findViewById(R.id.password_register_et);
        confirmPasswordEt = (TextInputLayout) view.findViewById(R.id.confirm_password_et);
        phoneNumberEt = (TextInputLayout) view.findViewById(R.id.phone_number_et);
        registerBtn = (Button) view.findViewById(R.id.register_btn);
        alreadyAccountTv.setOnClickListener(onClickListener);
        registerBtn.setOnClickListener(onClickListener);
        registerProgress = new ProgressDialog(mContext);
        registerProgress.setCanceledOnTouchOutside(false);
        registerProgress.setCancelable(false);
        registerProgress.setMessage(getString(R.string.waite_please));
    }
}
