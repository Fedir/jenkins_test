package com.camness.vidimguploadjun2016.upload;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.view.NestedScrollingParentHelper;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.search.CategoryItem;
import com.camness.vidimguploadjun2016.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CategoryUploadRecyclerAdapter extends RecyclerView.Adapter<CategoryUploadRecyclerAdapter.CategoryHolder> {
    private static final String TAG = "ChatRecyclerAdapter";
    private ListenerNestedScroll mListenerNestedScroll;
    private Context mContext;
    private List<CategoryItem> mDataList;
    private List<CategoryItem> realData;

    public CategoryUploadRecyclerAdapter(Context context, List<CategoryItem> dataItemList, ListenerNestedScroll listenerNestedScroll) {
        this.mContext = context;
        this.realData = new ArrayList<>(dataItemList);
        this.mDataList = new ArrayList<>();
        mListenerNestedScroll = listenerNestedScroll;
        mDataList.addAll(realData);
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new CategoryHolder(inflater.inflate(R.layout.category_upload_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(final CategoryHolder holder, final int position) {
        fillItemCaterogy(holder, position);
    }

    private void fillItemCaterogy(CategoryHolder holder, int position) {
        CategoryItem item = mDataList.get(position);
        holder.radioButton.setChecked(item.isChecked());
        holder.textView.setText(getNameString(item.getTitle(), item.getLevel()));
        if (item.hasSubItems()) {
            holder.radioButton.setVisibility(View.INVISIBLE);
            holder.arrowDropDownCategory.setVisibility(View.VISIBLE);
        } else {
            holder.radioButton.setVisibility(View.VISIBLE);
            holder.arrowDropDownCategory.setVisibility(View.GONE);
        }
        if (item.isExpanded()) {
            holder.arrowDropDownCategory.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_drop_up_arrow));
        } else {
            holder.arrowDropDownCategory.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_drop_down_arrow));
        }
    }

    private String getNameString(String title, int level) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level; i++) {
            sb.append("    ");
        }
        sb.append(" ");
        return sb.toString() + title;
    }

    @Override
    public int getItemCount() {
        return mDataList != null ? mDataList.size() : 0;
    }

    public String getSelectedCategories() { //TODO get only string
        return getSelected(getZeroLevelItems());
    }

    private List<CategoryItem> getZeroLevelItems() {
        List<CategoryItem> items = new ArrayList<>();
        for (CategoryItem item : mDataList) {
            if (item.getLevel() == 0) items.add(item);
        }
        return items;
    }

    private String getSelected(List<CategoryItem> items) {
        StringBuilder sb = new StringBuilder();
        for (CategoryItem item : items) {
            if (item.hasSubItems()) {
                sb.append(getSelected(item.getSubItems()));
            } else if (item.isChecked()) {
                if (item.getTitle().contains("/")) {
                    String[] singleCategory = item.getTitle().split("/");
                    sb.append(singleCategory[singleCategory.length - 1]);
                } else {
                    sb.append(item.getTitle());
                }
            }
        }
        return sb.toString();
    }

    private void collapseItems(List<CategoryItem> items) {
        Collections.reverse(items);
        List<CategoryItem> reversed = new ArrayList<>(items);
        Collections.reverse(items);
        for (int i = 0; i < items.size(); i++) {
            CategoryItem item = reversed.get(i);
            int index = mDataList.indexOf(item);
            if (item.isExpanded()) {
                collapseItems(item.getSubItems());
                item.setExpanded(false);
            }
            mDataList.remove(index);
            notifyItemRemoved(index);
        }
    }

    private void expandItems(int start, List<CategoryItem> items) {
        for (int i = 0; i < items.size(); i++) {
            mDataList.add(start, items.get(i));
//            mListenerNestedScroll.scrollScrollview();
            notifyItemInserted(start);
            start++;

        }
    }

    private void makeItemClick(int position, View view) {
        Utils.hideKeyboard(mContext, view);
//        mListenerNestedScroll.scrollScrollview(position);

        if (position < 0) {
            return;
        }
        CategoryItem item = mDataList.get(position);

        if (item.hasSubItems()) {


            if (item.isExpanded()) {
                collapseItems(item.getSubItems());
            } else {
                expandItems(position + 1, item.getSubItems());
                mListenerNestedScroll.scrollRecycler(position, item.getSubItems().size(), view.getHeight());
            }
            item.setExpanded(!item.isExpanded());
            notifyItemRangeChanged(0, mDataList.size());

        } else {
            clearAll();
            mDataList.get(position).setChecked(!item.isChecked());
            notifyItemChanged(position);
        }

    }

    private void clearAll() {
        for (int i = 0; i < mDataList.size(); i++) {
            mDataList.get(i).setChecked(false);
            notifyItemChanged(i);
        }
    }

    class CategoryHolder extends RecyclerView.ViewHolder {
        TextView textView;
        RadioButton radioButton;
        ImageView arrowDropDownCategory;


        CategoryHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.category_name_upload);
            radioButton = (RadioButton) itemView.findViewById(R.id.radio_button_category_upload);
            arrowDropDownCategory = (ImageView) itemView.findViewById(R.id.drop_down_arrow_upload);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    makeItemClick(getAdapterPosition(), view);
                }
            });

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    makeItemClick(getAdapterPosition(), view);
                }
            });
        }
    }
}
