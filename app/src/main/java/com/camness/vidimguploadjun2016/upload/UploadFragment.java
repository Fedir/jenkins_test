package com.camness.vidimguploadjun2016.upload;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.MyApplication;
import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.database.LocalDBManager;
import com.camness.vidimguploadjun2016.enumeration.FileTypeEnum;
import com.camness.vidimguploadjun2016.main.OnItemChangeCallback;
import com.camness.vidimguploadjun2016.model.Point;
import com.camness.vidimguploadjun2016.network.MapsApiService;
import com.camness.vidimguploadjun2016.network.response.RouteCamness;
import com.camness.vidimguploadjun2016.network.response.RouteResponse;
import com.camness.vidimguploadjun2016.network.response.User;
import com.camness.vidimguploadjun2016.search.CategoryItem;
import com.camness.vidimguploadjun2016.service.Constans;
import com.camness.vidimguploadjun2016.service.GPSTrackerService;
import com.camness.vidimguploadjun2016.service.UploadFileService;
import com.camness.vidimguploadjun2016.utils.Utils;
import com.bumptech.glide.Glide;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadFragment extends Fragment implements ListenerNestedScroll, OnMapReadyCallback {

    private static final String TAG = "UploadFragment";
    private final static String ROUTE_VIDEO_KEY = "route";
    private final static String URI_VIDEO_KEY = "uriUpload";
    private final static String FILE_TYPE_KEY = "fileType";
    private TextInputLayout descriptionEd;
    private Button uploadBtn;
    private EMVideoView videoView;
    private ProgressDialog uploadProgress;
    private ImageView imageView;
    private String uri;
    private RouteCamness routeCamness;
    private FileTypeEnum fileType;
    private View view;
    private Context mContext;
    private EditText mEditTextDescription;
    private OnItemChangeCallback onItemChangeCallback;
    private CategoryUploadRecyclerAdapter mAdapter;
    private RecyclerView mCategoryRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private NestedScrollView mNestedScrollView;
    private FrameLayout mFrameLayout;


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();

            switch (viewId) {
                case R.id.upload_btn: {
                    if (!Utils.isNetworkConnected(mContext)) {
                        Toast.makeText(mContext, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                    }
                    uploadFile();
                    Utils.hideKeyboard(getContext(), getActivity().getCurrentFocus());
                }
                break;
            }
        }
    };


    private Callback<RouteResponse> routeResponseCallback = new Callback<RouteResponse>() {
        @Override
        public void onResponse(Call<RouteResponse> call, Response<RouteResponse> response) {
            checkRouteResponse(response.body());
            uploadProgress.dismiss();


        }

        @Override
        public void onFailure(Call<RouteResponse> call, Throwable t) {
            uploadProgress.dismiss();
            if (!Utils.isNetworkConnected(mContext)) {
                Toast.makeText(mContext, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, getString(R.string.google_maps_not_available), Toast.LENGTH_SHORT).show();
            }

        }
    };


    public static UploadFragment newInstance(RouteCamness routeCamness, String uri, FileTypeEnum fileTypeEnum) {
        Bundle args = new Bundle();
        args.putParcelable(ROUTE_VIDEO_KEY, routeCamness);
        args.putString(URI_VIDEO_KEY, uri);
        args.putSerializable(FILE_TYPE_KEY, fileTypeEnum);
        UploadFragment fragment = new UploadFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_upload, container, false);
        initUI();
        setToolbar();
        getArgumentsBundle();
        initCategory();
        initMap();
        if (fileType == FileTypeEnum.VIDEO) {
            setVideo();

        } else if (fileType == FileTypeEnum.IMAGE) {
            setImage();
        } else if (fileType == FileTypeEnum.EMPTY) {
            setEmptyRout();
        }

        uploadBtn.setOnClickListener(onClickListener);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof OnItemChangeCallback) {
            onItemChangeCallback = (OnItemChangeCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        onItemChangeCallback = null;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        getActivity().onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void getArgumentsBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            routeCamness = bundle.getParcelable(ROUTE_VIDEO_KEY);
            uri = bundle.getString(URI_VIDEO_KEY);
            fileType = (FileTypeEnum) bundle.getSerializable(FILE_TYPE_KEY);
        }
    }

    private void setToolbar() {
        onItemChangeCallback.setTitleToolbar(getString(R.string.upload));
        onItemChangeCallback.setArrowToolbar();
        onItemChangeCallback.disabledSwipeDrawer();
    }

    private void uploadFile() {
        String category = mAdapter.getSelectedCategories();
        if (category.isEmpty()) {
            Toast.makeText(getContext(), R.string.choose_category, Toast.LENGTH_SHORT).show();
            return;
        }
//        ArrayList<Point> points = routeCamness.getPoints();
/*        ArrayList<Point> points = new ArrayList<>();
        points.add(new Point(40.692651, -73.958698 ));
        points.add(new Point(40.695048, -73.962569 ));
        points.add(new Point(40.697641, -73.954888));
        points.add(new Point(40.698934, -73.955103));
        points.add(new Point(40.699829, -73.953644));
        points.add(new Point(40.699772, -73.956712));
        points.add(new Point(40.699118, -73.956209));*/

        uploadProgress.show();
        getRoute(fileType);


    }

    private void checkRouteResponse(RouteResponse routeResponse) {
        if (routeResponse != null && routeResponse.routes.size() != 0 && routeResponse.getPoints().getOverview_polyline().getPoints() != null) {
            startServiceForUploadFile(routeResponse.getPoints().getPolyLine().getPoints());
            Toast.makeText(mContext, R.string.started_upload, Toast.LENGTH_SHORT).show();
            getActivity().onBackPressed();
        } else {
            Toast.makeText(mContext, getString(R.string.google_maps_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void startServiceForUploadFile(String points) {
        String category = mAdapter.getSelectedCategories();
        routeCamness.setVideoDescription(descriptionEd.getEditText().getText().toString());
        routeCamness.setCategory(category);
        User user = LocalDBManager.getUser();

        Intent fileUploadIntent = new Intent(getContext(), UploadFileService.class)
                .putExtra(Constans.ROUTE_KEY_SERVICE, routeCamness)
                .putExtra(Constans.USER_ID_KEY_SERVICE, user.getId())
                .putExtra(Constans.POINTS_KEY_SERVICE, points)
                .putExtra(Constans.FILE_URI_KEY_SERVICE, uri)
                .putExtra(Constans.FILE_TYPE_KEY_SERVICE, fileType);
        if (fileType == FileTypeEnum.IMAGE) {
            LocalDBManager.saveUploadInfo(routeCamness, uri, user.getId(), points, "IMAGE");
        } else if (fileType == FileTypeEnum.VIDEO) {
            LocalDBManager.saveUploadInfo(routeCamness, uri, user.getId(), points, "VIDEO");
        } else if (fileType == FileTypeEnum.EMPTY) {
            LocalDBManager.saveUploadInfo(routeCamness, uri, user.getId(), points, "EMPTY");
        }


        getActivity().startService(fileUploadIntent);
    }

    private void setImage() {
        imageView.setVisibility(View.VISIBLE);
        mFrameLayout.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);
        Glide.with(this).load(uri).placeholder(R.drawable.ic_ondemand_video_black_24dp).into(imageView);
    }

    private void setVideo() {
        videoView.setVisibility(View.VISIBLE);
        mFrameLayout.setVisibility(View.GONE);
        videoView.setVideoURI(Uri.parse(uri));
        videoView.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion() {
                videoView.setVideoURI(Uri.parse(uri));
            }
        });
        imageView.setVisibility(View.GONE);
    }

    private void setEmptyRout() {
        mFrameLayout.setVisibility(View.VISIBLE);
        videoView.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);
    }

    private void initCategory() {
        mCategoryRecyclerView = (RecyclerView) view.findViewById(R.id.category_upload_recycler_view);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mCategoryRecyclerView.setLayoutManager(mLinearLayoutManager);
        mAdapter = new CategoryUploadRecyclerAdapter(getActivity(), getCategoryData(), this);
        mCategoryRecyclerView.setAdapter(mAdapter);
    }

    private List<CategoryItem> getCategoryData() {
        List<CategoryItem> items = new ArrayList<>();
        items.add(getTrafficCategory());
        items.add(getBusinessCategory());
        items.add(getItems(0, "Social", getResources().getStringArray(R.array.Social)));
        return items;
    }

    private CategoryItem getBusinessCategory() {
        List<CategoryItem> items = new ArrayList<>();
        items.add(getItems(1, "WorkShop", new String[]{}));
        items.add(getItems(1, "Hotels", new String[]{}));
        items.add(getItems(1, "Food", new String[]{}));
        items.add(getItems(1, "Gas Station", getResources().getStringArray(R.array.Gas_Station)));
        items.add(getItems(1, "Car Dealership", new String[]{}));
        items.add(getItems(1, "Car Accessories", new String[]{}));
        items.add(getItems(1, "Car Rental", new String[]{}));
        return new CategoryItem(0, "Business", items);
    }

    private CategoryItem getTrafficCategory() {
        List<CategoryItem> items = new ArrayList<>();
        items.add(getItems(1, "Roads", getResources().getStringArray(R.array.Roads)));
        items.add(getItems(1, "Traffic Violation", getResources().getStringArray(R.array.Traffic_Violation)));
        items.add(getItems(1, "Driving", getResources().getStringArray(R.array.Driving)));
        items.add(getItems(1, "Vehicles", getResources().getStringArray(R.array.Vehicles)));
        items.add(getItems(1, "Others-Traffic", new String[]{}));
        return new CategoryItem(0, "Traffic", items);
    }

    private CategoryItem getItems(int level, String title, String[] array) {
        List<CategoryItem> items = new ArrayList<>();
        for (String string : array) {
            items.add(new CategoryItem(level + 1, string, new ArrayList<CategoryItem>()));
        }
        return new CategoryItem(level, title, items);
    }

    private void initUI() {
        mCategoryRecyclerView = (RecyclerView) view.findViewById(R.id.category_upload_recycler_view);
        mNestedScrollView = (NestedScrollView) view.findViewById(R.id.nested_scroll_view);
        descriptionEd = (TextInputLayout) view.findViewById(R.id.description_et);

        uploadBtn = (Button) view.findViewById(R.id.upload_btn);
        videoView = (EMVideoView) view.findViewById(R.id.video_view);
        imageView = (ImageView) view.findViewById(R.id.upload_image_view);
        mFrameLayout = (FrameLayout) view.findViewById(R.id.frame);
        uploadProgress = new ProgressDialog(mContext);
        uploadProgress.setCanceledOnTouchOutside(false);
        uploadProgress.setCancelable(false);
        uploadProgress.setMessage(getString(R.string.waite_please));
    }


    public void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void scrollRecycler(final int position, final int count, final int viewHeight) {
        final float y = mCategoryRecyclerView.getChildAt(position).getPivotY();

        mNestedScrollView.post(new Runnable() {
            @Override
            public void run() {


                mNestedScrollView.scrollBy(0, (int) mNestedScrollView.getPivotY() + (int) y - 2 * viewHeight);


            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (getCurrentLocation() != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(getCurrentLocation(), 13));
            googleMap.addMarker(new MarkerOptions().position(getCurrentLocation()));
        }
    }

    private LatLng getCurrentLocation() {
        GPSTrackerService gps = new GPSTrackerService(getActivity());
        Location location = gps.getLastKnownLocation();
        if (location != null) {
            return new LatLng(location.getLatitude(), location.getLongitude());
        } else return null;

    }

    private ArrayList<Point> getRoutePoints(FileTypeEnum fileType) {
        ArrayList<Point> sPoints;
        if (fileType == FileTypeEnum.IMAGE || fileType == FileTypeEnum.VIDEO) {
            sPoints = routeCamness.getPoints();
        } else {
            RouteCamness route = new RouteCamness();
            route.setPoints(new ArrayList<>(MyApplication.getRoutePoints()));
            if(!route.getPoints().isEmpty()){
                route = Utils.initRoute(route);
            }
            routeCamness = route;
            sPoints = routeCamness.getPoints();

        }
        return sPoints;

    }

    private void getRoute(FileTypeEnum fileType) {
        Point st;
        Point end;
        ArrayList<Point> sPoints = getRoutePoints(fileType);
        if (!getRoutePoints(fileType).isEmpty()) {
            if (fileType == FileTypeEnum.IMAGE) {
                end = sPoints.remove(sPoints.size() - 1);
                st = end;
                sPoints.clear();
            } else {
                st = sPoints.remove(0);
                if (sPoints.size() == 0) {
                    end = st;
                } else {
                    end = sPoints.remove(sPoints.size() - 1);
                }
            }
            MapsApiService.getRoute(st.getmLatitude()
                            + "," + st.getmLongitude(),
                    end.getmLatitude() + "," + end.getmLongitude(),
                    sPoints, routeResponseCallback);

        }
        else {
            uploadProgress.dismiss();
        }

    }
}