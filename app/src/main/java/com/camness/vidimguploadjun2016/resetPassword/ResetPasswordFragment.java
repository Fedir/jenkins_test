package com.camness.vidimguploadjun2016.resetPassword;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.camness.vidimguploadjun2016.R;
import com.camness.vidimguploadjun2016.network.CamnessApiService;
import com.camness.vidimguploadjun2016.network.CancelableCallback;
import com.camness.vidimguploadjun2016.utils.Utils;

import retrofit2.Call;
import retrofit2.Response;

public class ResetPasswordFragment extends Fragment {
    private View view;
    private TextInputLayout emailInput;
    private TextView backLogin;
    private Button resetPassword;
    private ProgressDialog resetProgress;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();

            switch (viewId) {
                case R.id.reset_password_btn: {
                    resetProgress.show();
                    Utils.hideKeyboard(getContext(),getActivity().getCurrentFocus());
                    resetPassword(emailInput.getEditText().getText().toString());
                }
                break;

                case R.id.reset_back_login_tv: {
                    getActivity().onBackPressed();
                    Utils.hideKeyboard(getContext(),getActivity().getCurrentFocus());
                }
                break;
            }
        }
    };

    public static ResetPasswordFragment newInstance() {
        Bundle args = new Bundle();
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_reset_password, container, false);
        initUI();

        resetPassword.setOnClickListener(onClickListener);
        backLogin.setOnClickListener(onClickListener);

        emailInput.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                emailInput.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    private void resetPassword(String email) {
        if(!TextUtils.isEmpty(email)) {
            if (Utils.isValidEmail(email)) {
                CamnessApiService.resetPassword(email, new CancelableCallback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        resetProgress.dismiss();
                        Toast.makeText(getActivity(), R.string.link_sent, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        resetProgress.dismiss();
                        Toast.makeText(getActivity(), R.string.link_sent, Toast.LENGTH_LONG).show();
                    }
                });

            } else {
                emailInput.setErrorEnabled(true);
                emailInput.setError(getString(R.string.email_incorrect));
                resetProgress.dismiss();
            }

        } else {
            emailInput.setErrorEnabled(true);
            emailInput.setError(getString(R.string.empty_email));
            resetProgress.dismiss();
        }
    }

    private void initUI() {
        emailInput = (TextInputLayout) view.findViewById(R.id.email_reset_password_et);
        resetPassword = (Button) view.findViewById(R.id.reset_password_btn);
        backLogin = (TextView) view.findViewById(R.id.reset_back_login_tv);
        resetProgress = new ProgressDialog(getContext());
        resetProgress.setCanceledOnTouchOutside(false);
        resetProgress.setCancelable(false);
        resetProgress.setMessage(getString(R.string.waite_please));
    }
}